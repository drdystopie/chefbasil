<?php
//convert hex to rgb
if ( !function_exists ('vegan_getbowtied_hex2rgb') ) {
	function vegan_getbowtied_hex2rgb($hex) {
		$hex = str_replace("#", "", $hex);
		
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return implode(",", $rgb); // returns the rgb values separated by commas
		//return $rgb; // returns an array with the rgb values
	}
}
if ( !function_exists ('vegan_custom_styles') ) {
	function vegan_custom_styles() {
		global $post;	
		
		ob_start();	
		?>
		
		<!-- ******************************************************************** -->
		<!-- * Theme Options Styles ********************************************* -->
		<!-- ******************************************************************** -->
			
		<style>

			/* check main color */ 
			<?php if ( vegan_get_config('main_color') != "" ) : ?>

				/* seting border color main */
				.widget-features.style2:hover .fbox-image,
				.widget-features.default .image-inner, .widget-features.default .icon-inner{
					border-color: <?php echo esc_html( vegan_get_config('main_color') ) ?>;
				}

				/* seting background main */
				.cart-icon .count,  .widget-features-box.widget.default,
				.widget-newletter.style1 .btn, .owl-controls .owl-dots .owl-dot.active, .widget-features-box.style2:hover .fbox-icon .inner,
				.widget_apus_vertical_menu .widget-vertical-menu ul, ul.nav.style2 > li.active, a.btn-theme
				{
					background: <?php echo esc_html( vegan_get_config('main_color') ) ?>;
				}
				/* setting color*/
				
				.navbar-nav.megamenu .dropdown-menu > li.active > a, .product-block.grid .groups-button .addcart > .add-cart > a.button:hover, .apus-copyright a, .apus-copyright .widget-social .social li > a:hover, .apus-footer .widget-title, .apus-footer .widgettitle, .apus-footer .widget-heading, .apus-products-list .price span.woocs_price_code span.woocommerce-Price-amount, a:hover, a:focus,
				.product-block.grid .groups-button > .quick-view:hover i, .navbar-nav.megamenu .dropdown-menu > li > a:hover, .navbar-nav.megamenu .dropdown-menu > li > a:active, .about .author-about, .hotline .tt-hotline, .widget-features-box.style2 .fbox-icon i, .newletters-2 .description, .contact-topbar-1 .textwidget .media .media-body .phone-info, .apus-topbar a:hover, .apus-footer a:hover, .apus-footer a:focus, .apus-footer a:active, .banner1 .tt-banner, .widget_apus_recent_post .media-post-layout .posts-list .entry-title a:hover,
				.widget_apus_recent_post .media-post-layout .posts-list .entry-create .author a, .woocommerce .star-rating::before,
				.product-block.list .yith-wcwl-add-button > a:hover, .product-block.list .yith-wcwl-add-button > a:active, .product-block.list .yith-wcwl-wishlistexistsbrowse > a:hover, .product-block.list .yith-wcwl-wishlistexistsbrowse > a:active,
				.archive-shop div.product .information .compare:hover, .archive-shop div.product .information .compare:active, .archive-shop div.product .information .add_to_wishlist:hover, .archive-shop div.product .information .add_to_wishlist:active, .archive-shop div.product .information .yith-wcwl-wishlistexistsbrowse > a:hover, .archive-shop div.product .information .yith-wcwl-wishlistexistsbrowse > a:active, .archive-shop div.product .information .yith-wcwl-wishlistaddedbrowse > a:hover, .archive-shop div.product .information .yith-wcwl-wishlistaddedbrowse > a:active, .apus-products-list .groups-button * i:hover, .product-block.grid:hover .name a, .apus-products-list .product-block:hover .name a, #apus-header.header-v3 .header-main .main-menu nav li.active > a, #apus-header.header-v3 .header-main .main-menu nav li:hover > a, #apus-header.header-v3 .header-main .main-menu nav li:active > a, #apus-header.header-v4 .header-main .main-menu nav li.active > a, #apus-header.header-v4 .header-main .main-menu nav li:hover > a, #apus-header.header-v4 .header-main .main-menu nav li:active > a, #apus-header.header-v5 .header-main .main-menu nav li.active > a, #apus-header.header-v5 .header-main .main-menu nav li:hover > a, #apus-header.header-v5 .header-main .main-menu nav li:active > a, #apus-header.header-v6 .header-main .main-menu nav li.active > a, #apus-header.header-v6 .header-main .main-menu nav li:hover > a, #apus-header.header-v6 .header-main .main-menu nav li:active > a
				{
					color: <?php echo esc_html( vegan_get_config('main_color') ) ?>;
				}
				/* setting border color*/
				.tagcloud a:hover,
				.tabs-v1 .nav-tabs li:focus > a:focus, .tabs-v1 .nav-tabs li:focus > a:hover, .tabs-v1 .nav-tabs li:focus > a,
				.tabs-v1 .nav-tabs li:hover > a:focus, .tabs-v1 .nav-tabs li:hover > a:hover, 
				.tabs-v1 .nav-tabs li:hover > a, .tabs-v1 .nav-tabs li.active > a:focus, 
				.tabs-v1 .nav-tabs li.active > a:hover, .tabs-v1 .nav-tabs li.active > a,
				.apus-filter .change-view.active,
				.apus-pagination a:hover,
				.apus-pagination span.current, .apus-pagination a.current,
				.widget .widget-title > span, .widget .widgettitle > span, .widget .widget-heading > span,
				.product-block.grid:hover,
				.widget-newletter.style1 .btn,
				.widget-newletter.style1 .form-control, .owl-controls .owl-dots .owl-dot, .widget-features-box.style2 .fbox-icon .inner, a.btn-theme{
					border-color: <?php echo esc_html( vegan_get_config('main_color') ) ?>;
				}
				/* setting important*/
				.style2 .product-block.grid:hover .price, .kc_single_image:hover .kc-image-overlay{
					color: <?php echo esc_html( vegan_get_config('main_color') ) ?> !important;
				}

				.kc_single_image:hover .kc-image-overlay{
					background: <?php echo esc_html( vegan_get_config('main_color') ) ?> !important;
				}
			<?php endif; ?>

			
			/* Custom CSS */
			<?php if ( vegan_get_config('custom_css') != "" ) : ?>
				<?php echo vegan_get_config('custom_css') ?>
			<?php endif; ?>

		</style>

	<?php
		$content = ob_get_clean();
		$content = str_replace(array("\r\n", "\r"), "\n", $content);
		$lines = explode("\n", $content);
		$new_lines = array();
		foreach ($lines as $i => $line) {
			if (!empty($line)) {
				$new_lines[] = trim($line);
			}
		}
		
		echo implode($new_lines);
	}
}

?>
<?php add_action( 'wp_head', 'vegan_custom_styles', 99 ); ?>