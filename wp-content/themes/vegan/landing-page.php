<?php /* Template Name: Landing page */ 

while ( have_posts() ) : the_post();
	//get_template_part( 'post-formats/content', get_post_format() );
endwhile;

?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Basil, livreur de plats cuisinés</title>
    <link href="<?php echo get_template_directory_uri().'/landing_page/style.css'; ?>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  </head>
  <body id='landing_page'>

    <form action="#" method="post" name="sign up for beta form">
    		<?php
				echo do_shortcode("[mc4wp_form id='227']");
			?>
    </form>
<script type="text/javascript">
jQuery( document ).ready(function() {
    if (navigator.userAgent.indexOf('Mac OS X') != -1) {
  jQuery("body").addClass("mac");
  } else {
    jQuery("body").addClass("pc");
  }
  });

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92786039-1', 'auto');
  ga('send', 'pageview');


</script>


  </body>
</html>