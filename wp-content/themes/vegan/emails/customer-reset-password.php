<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user       = get_user_by('login', $user_login);
if ( $user ) {
    $first_name = $user->first_name;
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>Bonjour <?php echo $first_name ?>,</p>

<p>Oui nous vous comprenons, trop de mot de passe, tue le mot de passe. Mais pas de panique, si vous souhaitez le changer, cliquez sur le lien suivant : 	<a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>">
			<?php _e( 'Click here to reset your password', 'woocommerce' ); ?></a></p>

<p>Et si vous n'avez pas demandé la réinitialisation de votre mot de passe, ignorez simplement cet e-mail.
Soyez rassuré, votre compte reste sécurisé.</p>


<p>L’équipe Basil</p>


<?php do_action( 'woocommerce_email_footer', $email ); ?>
