<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user_email = $user_login;
$user       = get_user_by('login', $user_login);
if ( $user ) {
    $first_name = $user->first_name;
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>Bonjour <?php echo $first_name ?>,</p>

<p>Merci pour votre inscription et soyez bienvenue chez Basil ! </p>

<p>Nous sommes heureux de vous compter parmi nous et de pouvoir vous servir tous les jours de bons plats frais maison !</p>

<p>Toute la team Basil est à votre disposition si vous avez des questions, vous pouvez nous joindre sur l’adresse suivante : Hello@chefbasil.fr</p>

<p>Basilement,</p>


<p>Laura</p>


<?php do_action( 'woocommerce_email_footer', $email );
