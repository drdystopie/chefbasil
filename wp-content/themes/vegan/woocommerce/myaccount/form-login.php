<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="row" id="customer_login">

	<div class="col-md-12 col-sm-12 col-xs-12">
<?php 
    $detect = new Mobile_Detect;
?>
<?php endif; ?>

		<h2>Vous avez déjà un compte</h2>

		<form method="post" class="login" role="form">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-group form-row form-row-wide">
				<input type="text" class="input-text form-control" name="username" id="username" placeholder="Adresse Email" />
			</p>
			<p class="form-group form-row form-row-wide">
				<input class="input-text form-control" type="password" name="password" id="password" placeholder="Mot de passe" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-group form-row">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<p for="rememberme" class='rememberme_container'>
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'vegan' ); ?>
				</p>
				<p class="form-group lost_password lost_password_container">
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'vegan' ); ?></a>
				</p>
				
			</p>

			<input type="submit" class='submit_login' name="login" placeholder="CONNECTEZ-VOUS" />

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>


<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="col-md-12 col-sm-12 col-xs-12">

		<h2 class="register_h2">Inscrivez-vous</h2>

		<form method="post" class="register widget" role="form">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="form-group form-row form-row-wide">
					<input type="text" class="input-text form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="form-group form-row form-row-wide">
				<input type="email" class="input-text form-control" name="email" id="reg_email" placeholder="ADRESSE EMAIL" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="form-group form-row form-row-wide">
					<input type="password" class="input-text form-control" name="password" id="reg_password" placeholder="MOT DE PASSE" />
				</p>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'vegan' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<p class="form-group form-row">
				<?php wp_nonce_field( 'woocommerce-register' ); ?>
				<input type="submit" class='submit_register' name="register" value="S'inscrire" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>