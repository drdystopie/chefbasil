<?php
/**
 * Lost password form
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php wc_print_notices(); ?>

<form method="post" class="lost_reset_password">

	<?php if( 'lost_password' == $args['form'] ) : ?>

        <p>Mot de passe perdu&nbsp;? Veuillez saisir votre identifiant ou votre adresse e-mail. Vous recevrez un lien par e-mail pour créer un nouveau mot de passe.</p>

        <p class="form-group">
            <label for="user_login">Email</label>
            <input class="input-text form-control" type="text" name="user_login" id="user_login" />
        </p>

	<?php else : ?>

        <p>Entrer un nouveau mot de passe</p>

        <p class="form-group">
            <label for="password_1">Nouveau mot de passe <span class="required">*</span></label>
            <input type="password" class="input-text form-control" name="password_1" id="password_1" />
        </p>
        <p class="form-group">
            <label for="password_2">Ré-entrez le nouveau mot de passe <span class="required">*</span></label>
            <input type="password" class="input-text form-control" name="password_2" id="password_2" />
        </p>

        <input type="hidden" name="reset_key" value="<?php echo isset( $args['key'] ) ? $args['key'] : ''; ?>" />
        <input type="hidden" name="reset_login" value="<?php echo isset( $args['login'] ) ? $args['login'] : ''; ?>" />

    <?php endif; ?>

    <!--<div class="clear"></div>-->

    <p class="form-group">
		<input type="hidden" name="wc_reset_password" value="true" />
		<input type="submit" class="button" value="<?php echo 'lost_password' == $args['form'] ? esc_html__( 'Réinitialiser le mot de passe', 'vegan' ) : esc_html__( 'Sauvegarder', 'vegan' ); ?>" />
	</p>

	<?php wp_nonce_field( $args['form'] ); ?>

</form>