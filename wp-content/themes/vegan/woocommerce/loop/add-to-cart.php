<?php
/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<div class="add-cart"><a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class=" %s btn btn-sm btn-primary btn-outline product_type_%s"><i class="mn-icon-929"></i><span class="title-cart">%s</span></a></div>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		esc_attr( isset( $class ) ? $class : 'add_to_cart_button btn btn-sm btn-primary btn-outline' ),
		esc_attr( $product->product_type ),
		esc_html( $product->add_to_cart_text() )
	),
$product );