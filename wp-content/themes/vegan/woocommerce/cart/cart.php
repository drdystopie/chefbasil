<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript">
jQuery(document ).ready(function()
{
	chefbasil_schedule = jQuery.cookie('chefbasil_schedule');
	chefbasil_schedule_to = parseInt(chefbasil_schedule) + 1800;

	var datefrom = moment.unix(chefbasil_schedule).format("H[H]mm");
	var dateto = moment.unix(chefbasil_schedule_to).format("H[H]mm");

	jQuery(".cart_hours").text("AUJOURD'HUI " + datefrom +" - "+dateto);
});

</script>

<div class='votre_commande'>VOTRE COMMANDE</div>

<div class='cart_hours'></div>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<table class="produits_checkout" cellspacing="0">
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		$length = count(WC()->cart->get_cart());
		$i = 1;

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

			$class = "";

			if($i == 1)
			{
				$class = 'first';
			}
			else if($i == $length)
			{
				$class = 'last';
			}

			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) )." ".$class; ?>">

					<td class="product-thumbnail">
						<?php
							$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($_product->id ), array(160,90) );

							echo "<img src='".$image_attributes[0]."' height='".$image_attributes[1]."' width='".$image_attributes[2]."' >";
						?>
					</td>

					<td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
						<?php
							echo($_product->get_title());
						?>
					</td>


					<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
							<?php echo $cart_item['quantity']; ?>
					</td>

					<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
						<?php
							echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</td>
				</tr>
				<?php
			}

			$i++;
		}

		do_action( 'woocommerce_cart_contents' );
		?>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>

<?php do_action( 'woocommerce_after_cart_table' ); ?>


<div class='bottom_checkout'>
</div>

</form>

<div class="cart-collaterals">

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>

<?php do_action( 'woocommerce_after_cart' ); ?>



