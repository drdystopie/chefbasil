<?php 
global $product;
$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($product->id ), array(570,324) );
$stock = $product->get_stock_quantity();
?>

<div class="product-block grid" data-product-id="<?php echo esc_attr($product->id); ?>">
    <div class="block-inner">
        <figure class="image">
            <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>" class="product-image quickview" data-productslug="<?php echo trim($product->post->post_name); ?>">
                <?php
                    /**
                    * woocommerce_before_shop_loop_item_title hook
                    *
                    * @hooked woocommerce_show_product_loop_sale_flash - 10
                    * @hooked woocommerce_template_loop_product_thumbnail - 10
                    */
                    //vegan_swap_images();

                    
                ?>


                <img src="<?php echo $image_attributes[0]; ?>" data-src="<?php echo $image_attributes[0]; ?>" class="attachment-shop-catalog unveil-image image-no-effect unveil-image <?php if($stock === 0){echo 'brightness';} ?>">



                <?php
                    if($stock === 0){
                ?>
                <div class='no_stock'>C'est fini pour aujourd'hui</div>
                <?php
                    }
                    else if($stock === 1)
                    {
                        echo "<div class='stock_warning'>Il n'en reste qu'un !</div>";
                    }
                    else if($stock === 2)
                    {
                        echo "<div class='stock_warning'>Il n'en reste que deux ! </div>";
                    }
                ?>
            </a>
            <img class='glass_product' src='<?php echo get_stylesheet_directory_uri()."/images/glass.png"; ?>'>
        </figure>
        <!--<div class="groups-button clearfix">
            <?php
            /*
                if( class_exists( 'YITH_WCWL' ) ) {
                    echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                }*/
            ?>

            <?php if (vegan_get_config('show_quickview', true)) { ?>
                <div class="quick-view">
                    <a href="<?php the_permalink(); ?>" class="quickview btn btn-primary" data-productslug="<?php echo trim($product->post->post_name); ?>">
                       <i class="mn-icon-41"> </i>
                    </a>
                </div>
            <?php } ?>
        </div>
        -->

    </div>
    <div class="caption">
        <div class="meta">
            <div class="infor">

                <h3 class="name"><?php the_title(); ?></h3>
                <div class="tags">
                <?php 
                    $terms = wp_get_post_terms( $product->id,'product_tag'); 
                    
                    foreach ($terms as $key => $term) {
                        echo "<span class='tag'>".$term->name."</span>";
                    }
                ?>
                </div>
                
                <div itemprop="description" class='description_product_home'>
                    <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
                </div>

            </div>
        </div>


        <div class='separator_grey'></div>
        <!--
        <div class="addcart">
            <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
            <?php
                $action_add = 'yith-woocompare-add-product';
                $url_args = array(
                    'action' => $action_add,
                   'id' => $product->id
                );
            ?>

        </div> 
        -->

        <?php echo qty_selector($product->id); ?>
                        <?php
                    /**
                    * woocommerce_after_shop_loop_item_title hook
                    *
                    * @hooked woocommerce_template_loop_rating - 5
                    * @hooked woocommerce_template_loop_price - 10
                    */
                    remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5);
                    do_action( 'woocommerce_after_shop_loop_item_title');
                ?>

    </div>
</div>
