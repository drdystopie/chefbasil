<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


$user_id = $order->user_id;
$user = get_user_by('id', $user_id);
if ( $user ) {
	$first_name = $user->first_name;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>Rebonjour <?php echo $first_name; ?>,</p>

<p>Encore merci d’avoir commandé chez Basil</p>

<p>Nous serions ravis de savoir ce qui vous a plu - ou pas d’ailleurs - </p>
<p>Alors si vous avez quelques secondes à nous accorder, n’hésitez pas à nous donner votre avis ! <i>Vous faites partie de l’aventure après tout :)</i></p>

<p>- Expérience lors de la prise de commande </p>

<p>- Qualité du service </p>

<p>- Qualité des plats </p>

<p>PS : Vous trouverez votre facture en pièce jointe.</p>
<?php

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
