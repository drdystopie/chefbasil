<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
	
	$user_id = $order->user_id;
	$user       = get_user_by('id', $user_id);
		if ( $user ) {
		    $first_name = $user->first_name;
		}


	$schedule = new DateTime();
	$schedule->setTimestamp(get_post_meta( $order->id, 'schedule', true ));

	$schedule_to = new DateTime();
	$schedule_to->setTimestamp(get_post_meta( $order->id, 'schedule', true ));
	$schedule_to->add(new DateInterval('PT30M'));

	$address = preg_replace( "/\r|\n/", "", $order->shipping_address_1." ".$order->shipping_address_1." ".$order->shipping_postcode." ".$order->shipping_city);
?>

<p>Bonjour <?php echo $first_name; ?>,</p>

<p>Merci d’avoir commandé chez Basil, le Chef était ravi de cuisiner pour vous aujourd’hui !
<p>Nous préparons soigneusement votre commande et vous adresserons un texto dès que votre Basil Biker sera en route.</p>
<p>Vous pourrez le retrouver à votre porte pour qu’il puisse continuer sa tournée :</p>
entre <?php echo $schedule->format('H:i');?>-<?php echo $schedule_to->format('H:i');?> au <?php echo $address; ?></p>


<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Emails::order_schema_markup() Adds Schema.org markup.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

?>

<p></p>
<p></p>
<p></p>
<p>Toute la team Basil vous souhaite un bon appétit ! </p>
<p>(Et ça, ça fait 4 Bon appétit d’un coup, juste pour vous)</p>


<?php
/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
