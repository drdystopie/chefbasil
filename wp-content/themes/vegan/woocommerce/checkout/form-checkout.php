<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'vegan' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

<div id='inclure_couvert'>
<label class="checkbox "><input type="checkbox" class="input-checkbox " name="couvert" id="couvert" value="1"> Inclure les couverts</label>
</div>

<div class='separator_product_checkout'></div>

<div id='address_informations_supplementaires'>
<textarea name="address_informations_supplementaires" id="address_informations_supplementaires" placeholder="Si vous habitez une résidence, veuillez nous préciser code et numéro de bâtiment devant lequel votre Basil Biker vous attendra."></textarea>
</div>

<div class='separator_product_checkout'></div>

<div class="clearfix">
	<div class="details-check">
	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
				<label for="billing_company" class="">Nom de l’entreprise</label>
				<input type="text" class="input-text " name="billing_company" id="billing_company" placeholder="" autocomplete="organization" value="">
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>
		
		<div class="separator_product_checkout"></div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		
	<?php endif; ?>
	</div>
	<div class="details-review">
		<div class="order-review">
			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>

			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>
	</div>	

</div>


	

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
