<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//E-commerce GA
?>
<script type="text/javascript">
  		ga('set', 'page', '/thankyou');
    	ga('send', 'pageview');

    <?php
	$items = $order->get_items();
	?>
	ga('ecommerce:addTransaction', {
		  'id': '<?php echo $order->id; ?>',                     // Transaction ID. Required.
		  'affiliation': 'Chef Basil',   // Affiliation or store name.
		  'revenue': '<?php echo $order->get_total(); ?>',               // Grand Total.
		  'shipping': '<?php echo $order->get_total_shipping(); ?>',                  // Shipping.
		  'tax': '<?php echo $order->get_total_tax(); ?>'                     // Tax.
		});
	<?php
	foreach ($items as $key => $item) {
		$product_cat = get_the_terms($item['product_id'],'product_cat');
			?>
			ga('ecommerce:addItem', {
			  'id': '<?php echo $order->id; ?>',                     // Transaction ID. Required.
			  'name': "<?php echo $item['name']; ?>",    // Product name. Required.
			  'sku': '<?php echo $item['product_id']; ?>',                 // SKU/code.
			  'category': '<?php echo $product_cat[0]->name; ?>',         // Category or variation.
			  'price': '<?php echo $item['line_total']/$item['qty']; ?>',                 // Unit price.
			  'quantity': '<?php echo $item['qty']; ?>'                   // Quantity.
			});
	<?php
	}
	?>
	ga('ecommerce:send');


	jQuery( document ).ready(function() {
  		jQuery('.woocommerce:first').hide();
	})
</script>


<?php
if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p class="woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

		<p class="woocommerce-thankyou-order-failed-actions">
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<p class="woocommerce-thankyou-order-received">Merci, c'est validé ! Il ne nous reste plus qu'à préparer ça avec soin et nous arrivons</p>

		<?php 
				$schedule = new DateTime();
				$schedule->setTimestamp(get_post_meta( $order->id, 'schedule', true ));

				$schedule_to = new DateTime();
				$schedule_to->setTimestamp(get_post_meta( $order->id, 'schedule', true ));
				$schedule_to->add(new DateInterval('PT30M'));

				echo '<div id="livraison_prevu">Livraison prévue entre ' . $schedule->format('H:i') . ' - '.$schedule_to->format('H:i').'</div>';


				$adresse_raw = get_post_meta($order->id);
				$adresse = $adresse_raw['_billing_address_1'][0]." ".$adresse_raw['_billing_postcode'][0];

				$email = $adresse_raw['_billing_email'][0];

				$telephone = $adresse_raw['_billing_phone'][0];

		?>




			<div id='info_commande'>
				<div class='info_commande_line_double'>
					<div class='info_commande_line_img'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/thank_you_nbcommande.png'></div>
					<div class='info_commande_line_text1'>Numéro de commmande</div>
					<div class='info_commande_line_text2'><?php echo $order->get_order_number(); ?></div>
				</div>

				<div class='info_commande_line_simple'>
					<div class='info_commande_line_img'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/thank_you_total.png'></div>
					<div class='info_commande_line_text1'>Total</div>
					<div class='info_commande_line_text2'><?php echo $order->get_formatted_order_total(); ?></div>
				</div>

				<div class='info_commande_line_simple'>
					<div class='info_commande_line_img'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/thank_you_adresse.png'></div>
					<div class='info_commande_line_text1'>Adresse</div>
					<div class='info_commande_line_text2'><?php echo $adresse; ?></div>
				</div>

				<div class='info_commande_line_simple'>
					<div class='info_commande_line_img'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/thank_you_mail.png'></div>
					<div class='info_commande_line_text1'>Mail</div>
					<div class='info_commande_line_text2'><?php echo $email; ?></div>
				</div>

				<div class='info_commande_line_simple'>
					<div class='info_commande_line_img'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/thank_you_tel.png'></div>
					<div class='info_commande_line_text1'>Numero de téléphone</div>
					<div class='info_commande_line_text2'><?php echo $telephone; ?></div>
				</div>
			</div>

			<div id='regalez_vous'>
				Regalez-vous ! 
			</div>

		<?php
		include dirname(__FILE__).'../../../inc/Mobile_Detect.php';
		$detect = new Mobile_Detect;
		if($detect->isMobile())
		{
			?><a class='button_thankyou' href='<?php echo site_url(); ?>'>Retour sur le site</a><?php
		}
		?>


	<?php endif; ?>


<?php else : ?>

	<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
