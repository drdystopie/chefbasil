<?php
    $product = new Wc_Product($post->ID);
    $stock = $product->get_stock_quantity();
?>

<script type="text/javascript">
    ga('set', 'page', '<?php echo "/produit/".$post->post_name; ?>');
    ga('send', 'pageview');
</script>


<div class="quickview-container">
    <div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class('product'); ?>>
    	<div id="single-product" class="product-info woocommerce">
    		<div class="row">
    			<div class="col-lg-12 col-md-12 col-sm-12">
    				<?php
    					/**
    					 * woocommerce_before_single_product_summary hook
    					 *
    					 * @hooked woocommerce_show_product_sale_flash - 10
    					 * @hooked woocommerce_show_product_images - 20
    					 */
    					//do_action( 'woocommerce_before_single_product_summary' );
    					//wc_get_template( 'single-product/product-image-carousel.php' );


                        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),array('786', '428' ) );
                        
                        $class = "";

                        if($stock === 0){$class = 'brightness';}

                        echo "<img src='".$thumbnail_src[0]."' width='786' height='428' class='".$class."'></img>";


                                if($stock === 0){
                                        ?>
                                        <div class='no_stock'>C'est fini pour aujourd'hui</div>
                                        <?php
                                            }
                                            else if($stock === 1)
                                            {
                                                echo "<div class='stock_warning_modal'>Il n'en reste qu'un !</div>";
                                            }
                                            else if($stock === 2)
                                            {
                                                echo "<div class='stock_warning_modal'>Il n'en reste que deux ! </div>";
                                            }
                                        ?>
    			</div>
    			<div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="summary entry-summary">
                        <?php
                            /**
                            * woocommerce_single_product_summary hook
                            *
                            * @hooked woocommerce_template_single_title - 5
                            * @hooked woocommerce_template_single_rating - 10
                            * @hooked woocommerce_template_single_price - 10
                            * @hooked woocommerce_template_single_excerpt - 20
                            * @hooked woocommerce_template_single_add_to_cart - 30
                            * @hooked woocommerce_template_single_meta - 40
                            * @hooked woocommerce_template_single_sharing - 50
                            */
                            ?>
                            <div class='modal_tile'>
                                <h1 itemprop="name" class="product_title entry-title"><?php echo $post->post_title; ?></h1>
                                <div class='tags'>
                                <?php 
                                    $terms = wp_get_post_terms( $post->ID,'product_tag');  

                                    foreach ($terms as $key => $term) {
                                        echo "<span class='tag'>".$term->name."</span>";
                                    }
                                ?>
                                </div>
                            </div>
                            <div itemprop="description" class="description_courte">
                                <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
                            </div>
                            <div class="separator_grey"></div>
                            <?php
                            //woocommerce_template_single_rating();
                            
                            
                            echo "<div class='description_modal'>";
                            woocommerce_template_single_excerpt();
                            echo "</div>";

                            echo "<div class='qty_price_modal'>";
                            echo qty_selector($post->ID); 
                            woocommerce_template_single_price();
                            echo "</div>";
                            
                            
                            //woocommerce_template_single_add_to_cart();
                            //woocommerce_template_single_meta();

                            // woocommerce_template_single_title();
                            // woocommerce_template_single_rating();
                            // woocommerce_template_single_price();
                            // woocommerce_template_single_excerpt();
                            ?>



                            <div class="cart">
                                <?php //woocommerce_template_loop_add_to_cart(); ?>
                            </div>
                        <?php
                            //woocommerce_template_single_meta();
                        ?>
                    </div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class='closeMagnificPopup'>x</div>
</div>

