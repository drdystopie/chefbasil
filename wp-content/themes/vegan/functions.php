<?php
/**
 * vegan functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Vegan
 * @since Vegan 1.0
 */

date_default_timezone_set('Europe/Paris');

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since vegan 1.0
 */
define( 'VEGAN_THEME_VERSION', '1.0' );
define( 'VEGAN_DEMO_MODE', false );
define( 'VEGAN_DEV_MODE', true );

if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

if ( ! function_exists( 'vegan_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Vegan 1.0
 */
function vegan_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on vegan, use a find and replace
	 * to change 'vegan' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'vegan', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'vegan' ),
		'topmenu'  => esc_html__( 'Top Menu', 'vegan' ),
		'social'  => esc_html__( 'Social Links Menu', 'vegan' ),
		'footer-menu'  => esc_html__( 'Footer Menu', 'vegan' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	add_theme_support( "woocommerce" );
	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = vegan_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'vegan_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	vegan_get_load_plugins();
}
endif; // vegan_setup
add_action( 'after_setup_theme', 'vegan_setup' );


/**
 * Load Google Front
 */
function vegan_fonts_url() {
    $fonts_url = '';

    /* Translators: If there are characters in your language that are not
    * supported by Montserrat, translate this to 'off'. Do not translate
    * into your own language.
    */
    $josefin = _x( 'on', 'josefin font: on or off', 'vegan' );
    $amatic    = _x( 'on', 'amatic font: on or off', 'vegan' );
    $playfair = _x( 'on', 'playfair font: on or off', 'vegan' );
 
    if ( 'off' !== $josefin || 'off' !== $amatic || 'off' !== $playfair ) {
        $font_families = array();
 
        if ( 'off' !== $josefin ) {
            $font_families[] = 'Josefin+Sans:400,700';
        }
        if ( 'off' !== $amatic ) {
            $font_families[] = 'Amatic+SC:400,700';
        }
        if ( 'off' !== $playfair ) {
            $font_families[] = 'Playfair+Display:400';
        }
 
        $query_args = array(
            'family' => ( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 		
 		$protocol = is_ssl() ? 'https:' : 'http:';
        $fonts_url = add_query_arg( $query_args, $protocol .'//fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}

function vegan_full_fonts_url() {  
	$protocol = is_ssl() ? 'https:' : 'http:';
	wp_enqueue_style( 'vegan-theme-fonts', vegan_fonts_url(), array(), null );
}
add_action('wp_enqueue_scripts', 'vegan_full_fonts_url');

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Vegan 1.1
 */
function vegan_javascript_detection() {
	wp_add_inline_script( 'vegan-typekit', "(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);" );
}
add_action( 'wp_enqueue_scripts', 'vegan_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Vegan 1.0
 */
function vegan_scripts() {
	// Load our main stylesheet.
	$css_folder = vegan_get_css_folder();
	$js_folder = vegan_get_js_folder();
	$min = vegan_get_asset_min();

	$css_path = $css_folder . '/template'.$min.'.css';
	wp_enqueue_style( 'vegan-template', $css_path, array(), '3.2' );
	wp_enqueue_style( 'vegan-style', get_template_directory_uri() . '/style.css', array(), '3.2' );
	//load font awesome
	wp_enqueue_style( 'font-awesome', $css_folder . '/font-awesome'.$min.'.css', array(), '4.5.0' );

	//load font monia
	wp_enqueue_style( 'font-monia', $css_folder . '/font-monia'.$min.'.css', array(), '1.8.0' );

	// load animate version 3.5.0
	wp_enqueue_style( 'animate-style', $css_folder . '/animate'.$min.'.css', array(), '3.5.0' );

	// load bootstrap style
	if( is_rtl() ){
		wp_enqueue_style( 'bootstrap', $css_folder . '/bootstrap-rtl'.$min.'.css', array(), '3.2.0' );
	}else{
		wp_enqueue_style( 'bootstrap', $css_folder . '/bootstrap'.$min.'.css', array(), '3.2.0' );
	}
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_style( 'perfect-scrollbar', $css_folder . '/perfect-scrollbar'.$min.'.css', array(), '2.3.2' );

	wp_enqueue_script( 'bootstrap', $js_folder . '/bootstrap'.$min.'.js', array( 'jquery' ), '20150330', true );
	wp_enqueue_script( 'owl-carousel', $js_folder . '/owl.carousel'.$min.'.js', array( 'jquery' ), '2.0.0', true );
	wp_enqueue_script( 'perfect-scrollbar-jquery', $js_folder . '/perfect-scrollbar.jquery'.$min.'.js', array( 'jquery' ), '2.0.0', true );

	wp_enqueue_script( 'jquery-magnific-popup', $js_folder . '/magnific/jquery.magnific-popup'.$min.'.js', array( 'jquery' ), '1.1.0', true );
	wp_enqueue_style( 'magnific-popup', $js_folder . '/magnific/magnific-popup'.$min.'.css', array(), '1.1.0' );
	
	// lazyload image
	wp_enqueue_script( 'jquery-unveil', $js_folder . '/jquery.unveil'.$min.'.js', array( 'jquery' ), '20150330', true );

	wp_register_script( 'vegan-functions', $js_folder . '/functions'.$min.'.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'vegan-functions', 'vegan_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_enqueue_script( 'vegan-functions' );

	if ( vegan_get_config('header_js') != "" ) {
		wp_add_inline_script( 'vegan-header', vegan_get_config('header_js') );
	}
}
add_action( 'wp_enqueue_scripts', 'vegan_scripts', 100 );

function vegan_footer_scripts() {
	if ( vegan_get_config('footer_js') != "" ) {
		wp_add_inline_script( 'vegan-footer', vegan_get_config('footer_js') );
	}
}
add_action('wp_footer', 'vegan_footer_scripts');
/**
 * Display descriptions in main navigation.
 *
 * @since Vegan 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function vegan_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'vegan_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Vegan 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function vegan_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'vegan_search_form_modify' );

/**
 * Function for remove srcset (WP4.4)
 *
 */
function vegan_disable_srcset( $sources ) {
    return false;
}
add_filter( 'wp_calculate_image_srcset', 'vegan_disable_srcset' );

/**
 * Function get opt_name
 *
 */
function vegan_get_opt_name() {
	return 'vegan_theme_options';
}
add_filter( 'apus_themer_get_opt_name', 'vegan_get_opt_name' );

function vegan_register_demo_mode() {
	if ( defined('VEGAN_DEMO_MODE') && VEGAN_DEMO_MODE ) {
		return true;
	}
	return false;
}
add_filter( 'apus_themer_register_demo_mode', 'vegan_register_demo_mode' );

function vegan_get_demo_preset() {
	$preset = '';
    if ( defined('VEGAN_DEMO_MODE') && VEGAN_DEMO_MODE ) {
        if ( isset($_GET['_preset']) && $_GET['_preset'] ) {
            $presets = get_option( 'apus_themer_presets' );
            if ( is_array($presets) && isset($presets[$_GET['_preset']]) ) {
                $preset = $_GET['_preset'];
            }
        } else {
            $preset = get_option( 'apus_themer_preset_default' );
        }
    }
    return $preset;
}

function vegan_cancel_import_content($return) {
	return true;
}
add_filter( 'apus_themer_cancel_import_content', 'vegan_cancel_import_content' );

function vegan_get_config($name, $default = '') {
	global $vegan_options;
    if ( isset($vegan_options[$name]) ) {
        return $vegan_options[$name];
    }
    return $default;
}

function vegan_get_global_config($name, $default = '') {
	$options = get_option( 'vegan_theme_options', array() );
	if ( isset($options[$name]) ) {
        return $options[$name];
    }
    return $default;
}

function vegan_get_image_lazy_loading() {
	return vegan_get_config('image_lazy_loading');
}

add_filter( 'apus_themer_get_image_lazy_loading', 'vegan_get_image_lazy_loading');

function vegan_register_sidebar() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Default', 'vegan' ),
		'id'            => 'sidebar-default',
		'description'   => esc_html__( 'Add widgets here to appear in your Sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Currency Switcher', 'vegan' ),
		'id'            => 'currency-switcher',
		'description'   => esc_html__( 'Add widgets here to appear in your Header.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Contact us Topbar 2', 'vegan' ),
		'id'            => 'contact-topbar-2',
		'description'   => esc_html__( 'Add widgets here to appear in your Top Bar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Contact us Topbar 1', 'vegan' ),
		'id'            => 'contact-topbar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your Top Bar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Vertical Menu', 'vegan' ),
		'id'            => 'sidebar-verticalmenu',
		'description'   => esc_html__( 'Add widgets here to appear in your Top Bar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Blog left sidebar', 'vegan' ),
		'id'            => 'blog-left-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Blog right sidebar', 'vegan' ),
		'id'            => 'blog-right-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Product left sidebar', 'vegan' ),
		'id'            => 'product-left-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Product right sidebar', 'vegan' ),
		'id'            => 'product-right-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Filter sidebar', 'vegan' ),
		'id'            => 'filter-sidebar',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'vegan' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );
	register_sidebar( array(
		'name' 				=> esc_html__( 'Shop', 'vegan' ),
		'id' 				=> 'widgets-shop',
		'before_widget'		=> '<div id="%1$s" class="col-md-cus-5 perfect-scroll  %2$s">',
		'after_widget' 		=> '</div></div></div>',
		'before_title' 		=> '<h3 class="apus-widget-title">',
		'after_title' 		=> '</h3><div class="apus-widget-content"><div class="apus-widget-scroll">'
	));
}
add_action( 'widgets_init', 'vegan_register_sidebar' );

/*
 * Init widgets
 */
function vegan_widgets_init($widgets) {
	$widgets = array_merge($widgets, array( 'woo-price-filter', 'woo-product-sorting', 'vertical_menu' ));
	return $widgets;
}
add_filter( 'apus_themer_register_widgets', 'vegan_widgets_init' );

function vegan_get_load_plugins() {
	// framework
	$plugins[] =(array(
		'name'                     => esc_html__( 'Apus Themer For Themes', 'vegan' ),
        'slug'                     => 'apus-themer',
        'required'                 => true,
        'source'				   => esc_url( 'http://apusthemes.com/themeplugins/apus-themer.zip' )
	));

	$plugins[] =(array(
		'name'                     => esc_html__( 'Cmb2', 'vegan' ),
	    'slug'                     => 'cmb2',
	    'required'                 => true,
	));
	
	$plugins[] =(array(
		'name'                     => esc_html__('King Composer - Page Builder', 'vegan'),
	    'slug'                     => 'kingcomposer',
	    'required'                 => true,
	));

	$plugins[] =(array(
		'name'                     => esc_html__( 'Revolution Slider', 'vegan' ),
        'slug'                     => 'revslider',
        'required'                 => true,
        'source'				   => esc_url( 'http://apusthemes.com/themeplugins/revslider.zip' )
	));

	// for woocommerce
	$plugins[] =(array(
		'name'                     => esc_html__( 'WooCommerce', 'vegan' ),
	    'slug'                     => 'woocommerce',
	    'required'                 => true,
	));

	$plugins[] =(array(
		'name'                     => esc_html__( 'YITH WooCommerce Wishlist', 'vegan' ),
	    'slug'                     => 'yith-woocommerce-wishlist',
	    'required'                 =>  true
	));

	$plugins[] =(array(
		'name'                     => esc_html__( 'WooCommerce Quantity Increment', 'vegan' ),
        'slug'                     => 'woocommerce-quantity-increment',
        'required'                 => false,
	));

	// for other plugins
	$plugins[] =(array(
		'name'                     => esc_html__( 'MailChimp for WordPress', 'vegan' ),
	    'slug'                     => 'mailchimp-for-wp',
	    'required'                 =>  true
	));

	$plugins[] =(array(
		'name'                     => esc_html__( 'Contact Form 7', 'vegan' ),
	    'slug'                     => 'contact-form-7',
	    'required'                 => true,
	));

	tgmpa( $plugins );
}

require get_template_directory() . '/inc/plugins/class-tgm-plugin-activation.php';
require get_template_directory() . '/inc/functions-helper.php';
require get_template_directory() . '/inc/functions-frontend.php';

/**
 * Implement the Custom Header feature.
 *
 */
require get_template_directory() . '/inc/custom-header.php';
require get_template_directory() . '/inc/classes/megamenu.php';
require get_template_directory() . '/inc/classes/mobilemenu.php';

/**
 * Custom template tags for this theme.
 *
 */
require get_template_directory() . '/inc/template-tags.php';


if ( defined( 'APUS_THEMER_REDUX_ACTIVED' ) ) {
	require get_template_directory() . '/inc/vendors/redux-framework/redux-config.php';
	define( 'VEGAN_REDUX_THEMER_ACTIVED', true );
}
if( in_array( 'cmb2/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require get_template_directory() . '/inc/vendors/cmb2/page.php';
	require get_template_directory() . '/inc/vendors/cmb2/footer.php';
	define( 'VEGAN_CMB2_ACTIVED', true );
}
if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require get_template_directory() . '/inc/vendors/woocommerce/functions.php';
	define( 'VEGAN_WOOCOMMERCE_ACTIVED', true );
}
if( in_array( 'kingcomposer/kingcomposer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require get_template_directory() . '/inc/vendors/kingcomposer/functions.php';
	require get_template_directory() . '/inc/vendors/kingcomposer/maps.php';
	define( 'VEGAN_KINGCOMPOSER_ACTIVED', true );
}
if( in_array( 'apus-themer/apus-themer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	require get_template_directory() . '/inc/widgets/popup_newsletter.php';
	require get_template_directory() . '/inc/widgets/popup_promotion.php';
}
/**
 * Customizer additions.
 *
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Styles
 *
 */
require get_template_directory() . '/inc/custom-styles.php';


/* 
 * get all possible schedule 
 * delete all already booked schedules
*/
function getAllPossibleSchedule()
{	
	global $wpdb;

	$sql_matin = 'SELECT * FROM schedules where matin_soir = "matin" order by id';
	$results_schedules_matin = $wpdb->get_results( $sql_matin, OBJECT );


	foreach ($results_schedules_matin as $key => $result) {
		{
			$schedules['matin'][] = ["date_from" => new DateTime($result->hour_from),"date_to" => new DateTime($result->hour_to),"Availible" => $result->Availible];
		}
		
	}

	$sql_soir = 'SELECT * FROM schedules where matin_soir = "soir" order by id';
	$results_schedules_matin = $wpdb->get_results( $sql_soir, OBJECT );

	foreach ($results_schedules_matin as $key => $result) {
		{
			$schedules['soir'][] = ["date_from" => new DateTime($result->hour_from),"date_to" => new DateTime($result->hour_to),"Availible" => $result->Availible];
		}
		
	}


	//On recherche dans la base de données toutes les dates deja booké en timestamp
	$in_schedule = "";
	$array_key = array_keys($schedules['soir']);
	foreach ($schedules['soir'] as $key => $schedule) 
	{	
		if($schedule['date_from'] != '===')
		{
			if(end($array_key) == $key)
			{
				$in_schedule .= $schedule['date_from']->getTimestamp();
			}
			else
			{
				$in_schedule .= $schedule['date_from']->getTimestamp().",";
			}
		}
		
	}

	$sql = "select date_from,COUNT(date_from) as count from schedules_booked where date_from in (".$in_schedule.") group by date_from";
	$results_schedules_booked = $wpdb->get_results( $sql , OBJECT );


	foreach ($results_schedules_booked as $key => $result_schedules_booked) {
			
		foreach ($schedules['soir'] as $key => $schedule) {

			if($schedule['date_from']->getTimestamp() == $result_schedules_booked->date_from && $result_schedules_booked->count >= $schedule['Availible'])
			{
				$schedules['soir'][$key]['desactivate'] = true;
			}
		}
	}

	$in_schedule = "";
	$array_key = array_keys($schedules['soir']);
	foreach ($schedules['matin'] as $key => $schedule) 
	{	
		if($schedule['date_from'] != '===')
		{
			if(end($array_key) == $key)
			{
				$in_schedule .= $schedule['date_from']->getTimestamp();
			}
			else
			{
				$in_schedule .= $schedule['date_from']->getTimestamp().",";
			}
		}
		
	}

	$sql = "select date_from,COUNT(date_from) as count from schedules_booked where date_from in (".$in_schedule.") group by date_from";
	$results_schedules_booked = $wpdb->get_results( $sql , OBJECT );


	foreach ($results_schedules_booked as $key => $result_schedules_booked) {
		foreach ($schedules['matin'] as $key => $schedule) {
			if($schedule['date_from']->getTimestamp() == $result_schedules_booked->date_from && $result_schedules_booked->count >= $schedule['Availible'])
			{
				$schedules['matin'][$key]['desactivate'] = true;
			}
		}
	}

	return $schedules;
}


/* AJOUT CHAMP CHECKOUT */
// Hook in
add_action( 'woocommerce_after_order_notes', 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $checkout ) {

	$schedules = getAllPossibleSchedule();


	$schedulesForSelect = array();

	foreach ($schedules['matin'] as $key => $schedule) {
		$schedulesForSelect[$schedule['date_from']->getTimestamp()] = $schedule['date_from']->format('H:i');
	}

	foreach ($schedules['soir'] as $key => $schedule) {
		$schedulesForSelect[$schedule['date_from']->getTimestamp()] = $schedule['date_from']->format('H:i');
	}
   

    woocommerce_form_field( 'schedule', array(
        'type'          => 'select',
        'options' => $schedulesForSelect,
        'class'         => array('form-row-wide'),
        ), $checkout->get_value( 'schedule' ));
}

add_action('woocommerce_checkout_process', 'checkout_schedule_field');

function checkout_schedule_field() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['schedule'] && $_POST['schedule'] != "Choisir un horaire de livraison")
        wc_add_notice( __( 'Veuillez choisir un horaire de livraison.' ), 'error' );
}

add_action( 'woocommerce_checkout_update_order_meta', 'checkout_field_update_order_meta_schedule' );

function checkout_field_update_order_meta_schedule( $order_id ) {
    if ( ! empty( $_POST['schedule'] ) ) {
        update_post_meta( $order_id, 'schedule', sanitize_text_field( $_POST['schedule'] ) );

        update_post_meta( $order_id, 'couvert', sanitize_text_field( $_POST['couvert'] ) );

        update_post_meta( $order_id, 'address_informations_supplementaires', implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST['address_informations_supplementaires'] ) ) ) );

        global $wpdb;

        	$wpdb->insert( 
				'schedules_booked', 
				array( 
					'date_from' => $_POST['schedule']
				),
				array('%s')
			);
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'schedule_checkout_field_display_admin_order_meta', 10, 1 );

function schedule_checkout_field_display_admin_order_meta($order){

	$address_informations_supplementaires = get_post_meta( $order->id, 'address_informations_supplementaires', true );

	$schedule = new DateTime();
	$schedule->setTimestamp(get_post_meta( $order->id, 'schedule', true ));

	$schedule_to = new DateTime();
	$schedule_to->setTimestamp(get_post_meta( $order->id, 'schedule', true ));
	$schedule_to->add(new DateInterval('PT30M'));

	$couvert = get_post_meta( $order->id, 'couvert', true );
	if($couvert == '1')
	{
		$couvert = 'oui';
	}
	else
	{
		$couvert = 'non';
	}

    echo '<p><strong>'.__('Horaire').':</strong> ' . $schedule->format('H:i') . ' à '.$schedule_to->format('H:i').'</p>';

    echo '<p><strong>'.__('Couvert').':</strong> ' . $couvert.'</p>';

    echo '<p><strong>'.__('informations supplémentaires').':</strong><br> ' . nl2br($address_informations_supplementaires).'</p>';
}

/* adding static content to menu */

add_filter('wp_nav_menu_items','search_box_function', 10, 2);
function search_box_function( $nav, $args ) {
    if( $args->theme_location == 'primary' )
        return "<li class='menu-item-fa rs_li'><a href='https://www.facebook.com/BASIL-1173555699422258' target='_blank' class='icon_facebook'></a></li>"."<li class='menu-item-fa rs_li'><a href='https://www.instagram.com/basilmarseille/' target='_blank' class='icon_instagram'></a></li>"."<li class='menu-item-fa rs_li'><a href='https://twitter.com/chefbasilnews' target='_blank' class='icon_twitter'></a></li>".$nav;

    return $nav;
}

/* QTY PRODUCT SELECTOR */
function qty_selector($product_id)
{
	global $woocommerce;

	$product = new WC_Product($product_id);
	$stock = $product->get_stock_quantity();


	if(isset($woocommerce->cart))
	{
		
    	$products = $woocommerce->cart->get_cart();

		$actualQty = 0;	
    	
    if(isset($products))
    {
    	    foreach($products as $item => $values) { 
    		$_product = $values['data']->post;
    		if($values['product_id'] == $product_id)
    		{
    			$actualQty = ($values['quantity']);
    		}
        }
    }


    $dayNumber = date('w');

    global $wpdb;
    $option = $wpdb->get_row("SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend' LIMIT 1");
    $option = $option->option_value;



    if(!current_user_can('administrator'))
    {
    	    if($stock === 0 || $dayNumber == 6 || $dayNumber == 0 || $option == "1")
    	{
    	$product_id = 0;
    	}
    }

	return '<div class="container_qty">
            <div class="container_qty_remove" data-id="'.$product_id .'">
            </div>
            <div class="container_qty_qty" data-id="'.$product_id.'">'.$actualQty.'</div>
            <div class="container_qty_add" data-id="'.$product_id.'">
            </div>
        </div>';
	}

}

/* */
function wooc_extra_register_fields() {?>
       <p class="form-row form-row-wide">
       <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" required placeholder="Prénom" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
       </p>
       <p class="form-row form-row-wide">
       <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" required placeholder="Nom" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
       </p>
       <p class="form-row form-row-wide">
       <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" required placeholder="Téléphone portable" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
       </p>
       <div class="clear"></div>
       <?php
 }
 add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
/**
* Below code save extra fields.
*/
function wooc_save_extra_register_fields( $customer_id ) {
    if ( isset( $_POST['billing_phone'] ) ) {
                 // Phone input filed which is used in WooCommerce
                 update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
          }

      if ( isset( $_POST['billing_first_name'] ) ) {
             //First name field which is by default
             update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
             // First name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
      }
      if ( isset( $_POST['billing_last_name'] ) ) {
             // Last name field which is by default
             update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
             // Last name field which is used in WooCommerce
             update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
      }
 
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );


// Add a second password field to the checkout page.
add_action( 'woocommerce_checkout_init', 'wc_add_confirm_password_checkout', 10, 1 );
function wc_add_confirm_password_checkout( $checkout ) {
    if ( get_option( 'woocommerce_registration_generate_password' ) == 'no' ) {
        $checkout->checkout_fields['account']['account_password2'] = array(
            'type'              => 'password',
            'label'             => __( 'Confirm password', 'woocommerce' ),
            'required'          => true,
            'placeholder'       => _x( 'Confirm Password', 'placeholder', 'woocommerce' )
        );
    }
}
// Check the password and confirm password fields match before allow checkout to proceed.
add_action( 'woocommerce_after_checkout_validation', 'wc_check_confirm_password_matches_checkout', 10, 2 );
function wc_check_confirm_password_matches_checkout( $posted ) {
    $checkout = WC()->checkout;
    if ( ! is_user_logged_in() && ( $checkout->must_create_account || ! empty( $posted['createaccount'] ) ) ) {
        if ( strcmp( $posted['account_password'], $posted['account_password2'] ) !== 0 ) {
            wc_add_notice( __( 'Passwords do not match.', 'woocommerce' ), 'error' );
        }
    }
}

// Add the code below to your theme's functions.php file to add a confirm password field on the register form under My Accounts.
add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);
function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	extract( $_POST );


	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}

	return $reg_errors;
}


add_action( 'woocommerce_process_registration_errors', 'phone_registration_error' );

function phone_registration_error( $validation_error ){

	$pattern = '/^0(6|7)[0-9]{8}$/';
	
	extract($_POST);

	if(!preg_match($pattern, $billing_phone))
	{
		$validation_error = new WP_Error( 'phone-error', 'Numéro de téléphone invalide');
	}


    return $validation_error;
}







add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );
function wc_register_form_password_repeat() {
	?>
	<p class="form-row form-row-wide">
		<input type="password" class="input-text" name="password2" id="reg_password2" placeholder="CONFIRMEZ LE MOT DE PASSE" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
	</p>
	<?php
}


function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'yes':'no';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');


function mode_theme_update_mini_cart() {
    echo wc_get_template( 'cart/mini-cart.php' );
    die();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );



function ajax_qty()
{
	global $woocommerce;

	$id_product = filter_var ( $_GET['id_product'],FILTER_SANITIZE_NUMBER_INT );
	$qty = filter_var ( $_GET['qty'],FILTER_SANITIZE_NUMBER_INT );

	$founded = false;

	foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
    	if($cart_item['product_id'] == $id_product)
    	{
			$woocommerce->cart->set_quantity($cart_item_key, $cart_item['quantity']+$qty);
			$founded = true;
    	}

	}

	if($qty > 0 && !$founded)
	{
		$woocommerce->cart->add_to_cart($id_product);
			foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
	    	if($cart_item['product_id'] == $id_product)
	    	{
				$woocommerce->cart->set_quantity($cart_item_key, $cart_item['quantity']+$qty-1);
				$founded = true;
	    	}

		}
	}

	echo $woocommerce->cart->get_cart_contents_count();

	die;
}

add_filter( 'wp_ajax_nopriv_ajax_qty', 'ajax_qty' );
add_filter( 'wp_ajax_ajax_qty', 'ajax_qty' );

function ajax_remove_product()
{
	global $woocommerce;
	$id_product = filter_var ( $_GET['id_product'],FILTER_SANITIZE_STRING );

	foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
    	if($cart_item['product_id'] == $id_product)
    	{
			$woocommerce->cart->set_quantity($cart_item_key, 0);
			$founded = true;
    	}

	}

	echo $woocommerce->cart->get_cart_contents_count();
	
	die();
}

add_filter( 'wp_ajax_nopriv_ajax_remove_product', 'ajax_remove_product' );
add_filter( 'wp_ajax_ajax_remove_product', 'ajax_remove_product' );


function ajax_get_count_cart()
{
	global $woocommerce;
	echo $woocommerce->cart->get_cart_contents_count();
	
	die();
}

add_filter( 'wp_ajax_nopriv_ajax_get_count_cart', 'ajax_get_count_cart' );
add_filter( 'wp_ajax_ajax_get_count_cart', 'ajax_get_count_cart' );


function ajax_get_cart()
{
	global $woocommerce;
	var_dump($woocommerce->cart);
	
	die();
}

add_filter( 'wp_ajax_nopriv_ajax_get_cart', 'ajax_get_cart' );
add_filter( 'wp_ajax_ajax_get_cart', 'ajax_get_cart' );


function fill_field_checkout( $atts ) {
	?>
	<script type="text/javascript">
		jQuery( document ).ready(function() {
			jQuery("#billing_address_1").val(jQuery.cookie('chefbasil_addr_billing_lineOne'));
			jQuery("#billing_city").val(jQuery.cookie('chefbasil_addr_billing_city'));
			jQuery("#billing_postcode").val(jQuery.cookie('chefbasil_addr_billing_postalCode'));

			jQuery("#schedule").val(jQuery.cookie('chefbasil_schedule'));
			
		});

	</script>
	
	<?php
}
add_shortcode( 'fill_field_checkout', 'fill_field_checkout' );



/* redirection login/inscription */

add_filter('woocommerce_login_redirect', 'wc_login_redirect');
 
function wc_login_redirect( $redirect_to ) {
     $redirect_to = home_url();
     return $redirect_to;
}

add_filter('woocommerce_registration_redirect', 'ps_wc_registration_redirect');
function ps_wc_registration_redirect( $redirect_to ) {

	 $current_user = wp_get_current_user();
    /**
     * @example Safe usage: $current_user = wp_get_current_user();
     * if ( !($current_user instanceof WP_User) )
     *     return;
     */

    $data = [
	    'email'     => $current_user->user_email,
	    'status'    => 'subscribed',
	    'firstname' => $current_user->user_firstname,
	    'lastname'  => $current_user->user_lastname
	];

    $email = $current_user->user_firstname;

    // MailChimp API credentials
        $apiKey = 'b55ee24012fc8dd350febbb76eddbc32-us15';
        $listID = '41dcc19eab';
        
        // MailChimp API URL
        $memberID = md5(strtolower($data['email']));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        
        // member information
        $json = json_encode([
            'email_address' => $data['email'],
            'status'        => 'subscribed',
            'merge_fields'  => [
                'FNAME'     => $data['firstname'],
                'LNAME'     => $data['lastname']
            ]
        ]);
        
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

     $redirect_to = home_url();
     return $redirect_to;
}

function shortcode_rs( $atts ) {
	?>
<a href="https://www.facebook.com/BASIL-1173555699422258" target="_blank"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/02/facebook_simple.png" data-src="http://chefbasil.fr/wp-content/uploads/2017/02/facebook_simple.png" alt="" class="unveil-image"></a>

<a href="https://www.instagram.com/basilmarseille/" target="_blank"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/02/instagram_simple.png" data-src="http://chefbasil.fr/wp-content/uploads/2017/02/instagram_simple.png" alt="" class="unveil-image"></a>

<a href="https://twitter.com/chefbasilnews" target="_blank"><img src="<?php echo site_url(); ?>/wp-content/uploads/2017/02/twitter_simple.png" data-src="http://chefbasil.fr/wp-content/uploads/2017/02/twitter_simple.png" alt="" class="unveil-image"></a>
	
	<?php
}
add_shortcode( 'shortcode_rs', 'shortcode_rs' );

/* MY ACCOUNT */

function wpb_woo_my_account_order() {
 $myorder = array(
 'orders' => "Mes commandes",
 'edit-account' => "Details du compte",
 'payment-methods' => __( 'Payment Methods', 'woocommerce' )
 );
 return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'wpb_woo_my_account_order' );



add_action( 'wp_enqueue_scripts', 'custom_cart_script' );
function custom_cart_script() {
    wp_dequeue_script( 'checkout.min' );
    wp_enqueue_script( 'checkout-new', get_stylesheet_directory_uri() . '/woocommerce/assets/checkout-new.js', array( 'jquery' ) );
}

/* supprimer les notifications de thèmes */
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) ); 
/* supprimer les notifications de plugins */
/*remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );*/


function add_css() {
    wp_enqueue_style( 'responsive_override', get_stylesheet_directory_uri()."/css/responsive_override.css" );
}
add_action( 'wp_enqueue_scripts', 'add_css' );

/** 
 * Register new status
 * Tutorial: http://www.sellwithwp.com/woocommerce-custom-order-status-2/
**/
function register_awaiting_shipment_order_status() {
    register_post_status( 'wc-awaiting-shipment', array(
        'label'                     => 'Awaiting shipment',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Commande prise en charge par le livreur <span class="count">(%s)</span>', 'Commande prise en charge par le livreur <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', 'register_awaiting_shipment_order_status' );

// Add to list of WC Order statuses
function add_awaiting_shipment_to_order_statuses( $order_statuses ) {

    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-awaiting-shipment'] = 'Commande prise en charge par le livreur';
        }
    }

    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_awaiting_shipment_to_order_statuses' );

add_action("woocommerce_order_status_changed", "add_horraire_to_notes");

function add_horraire_to_notes($order_id, $checkout=null) {

	$note = "";

	$data = get_post_meta( $order_id );


	if($data['couvert'][0] == 1)
	{
		$note .= "| Couvert : Oui";
	}
	else
	{
		$note .= "| Couvert : Non";
	}

	$schedule = new DateTime();
	$schedule->setTimestamp($data['schedule'][0]);

	$schedule_to = new DateTime();
	$schedule_to->setTimestamp($data['schedule'][0]);
	$schedule_to->add(new DateInterval('PT30M'));
	

	$note.= "| ".$schedule->format('H:i')." - ".$schedule_to->format('H:i')." | ".$data['address_informations_supplementaires'][0]." | ".$data['_payment_method_title'][0]." | ".$data['_order_total'][0]."€";

	// specify the order_id so WooCommerce knows which to update
	$order_data = array(
	    'order_id' => $order_id,
	    'customer_note' => $note
	);
	// update the customer_note on the order
	wc_update_order( $order_data );



	$invoice = new BEWPI_Invoice($order_id);

	$invoice->save();
}


// Register new status
/*function register_order_confirmed_order_status() {
    register_post_status( 'wc-order-prise-en-charge', array(
        'label' => 'Commande prise en charge par le livreur',
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop( 'Commande prise en charge par le livreur <span class="count">(%s)</span>', 'Commande prise en charge par le livreur <span class="count">(%s)</span>' )
    ) );
}


add_action( 'init', 'register_order_confirmed_order_status' );

// Add to list of WC Order statuses
function add_order_confirmed_to_order_statuses( $order_statuses ) {
    $new_order_statuses = array();
// add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-order-prise-en-charge'] = 'Commande prise en charge par le livreur';
        }
    }
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_order_confirmed_to_order_statuses' );


// Register new status
/*function register_order_commande_traitee() {
    register_post_status( 'wc-order-sms-traite', array(
        'label' => 'Commande prise en charge par le livreur',
        'public' => true,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true,
        'label_count' => _n_noop( 'Commande Traitée <span class="count">(%s)</span>', 'Commande Traitée <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', 'register_order_commande_traitee' );*/

// Add to list of WC Order statuses
/*function add_commande_traitee_to_order_statuses( $order_statuses ) {
    $new_order_statuses = array();
// add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
        $new_order_statuses[ $key ] = $status;
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-order-sms-traite'] = 'Commande Traitée';
        }
    }
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_commande_traitee_to_order_statuses' );*/




add_action("woocommerce_order_status_changed", "send_sms");

use Twilio\Rest\Client;

function send_sms($order_id, $checkout=null) {
   global $woocommerce;
   $order = new WC_Order( $order_id );

   /*if($order->status === 'wc-order-sms-prise-en-charge' ) {

   		require __DIR__ . '/Twilio/autoload.php';
		// Use the REST API Client to make requests to the Twilio REST API
		
		$sid = 'AC6866bfe118fbbfe6c6228279a49745b7';
		$token = '4df16acb865697cc91c719e38b7d17b4';
		$client = new Client($sid, $token);

		// Use the client to do fun stuff like send text messages!
		$client->messages->create(
		    // the number you'd like to send the message to
		    $order->billing_phone,
		    array(
		        // A Twilio phone number you purchased at twilio.com/console
		        'from' => '33644647538',
		        // the body of the text message you'd like to send
		        'body' => "Commande traitée, un livreur va prendre en charge votre commande"
		    )
		);

   }

   if($order->status === 'wc-order-sms-traite' ) {

   		require __DIR__ . '/Twilio/autoload.php';
		// Use the REST API Client to make requests to the Twilio REST API
		
		$sid = 'AC6866bfe118fbbfe6c6228279a49745b7';
		$token = '4df16acb865697cc91c719e38b7d17b4';
		$client = new Client($sid, $token);

		// Use the client to do fun stuff like send text messages!
		$client->messages->create(
		    // the number you'd like to send the message to
		    $order->billing_phone,
		    array(
		        // A Twilio phone number you purchased at twilio.com/console
		        'from' => '33644647538',
		        // the body of the text message you'd like to send
		        'body' => "Votre livreur est en cours de livraison"
		    )
		);

   }*/

   if($order->status === 'awaiting-shipment')
   {	
   		header('Location: '.get_site_url()."/sendToOnfleet.php?order_id=".$order_id);
   		die;
   }

}


/* Colonne Order */
add_filter( 'manage_edit-shop_order_columns', 'add_columns' );
function add_columns($columns){
    $new_columns = (is_array($columns)) ? $columns : array();
    unset( $new_columns['order_actions'] );

    //edit this for you column(s)
    //all of your columns will be added before the actions column
    $new_columns['horaire'] = 'Horaire';
    //stop editing

    $new_columns['order_actions'] = $columns['order_actions'];
    return $new_columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'display_columns', 2 );
function display_columns($column){
    global $post;
    $data = get_post_meta( $post->ID );

    //start editing, I was saving my fields for the orders as custom post meta
    //if you did the same, follow this code
    if ( $column == 'horaire' ) {    
        	$schedule = new DateTime();
			$schedule->setTimestamp(get_post_meta( $post->ID, 'schedule', true ));

			$schedule_to = new DateTime();
			$schedule_to->setTimestamp(get_post_meta( $post->ID, 'schedule', true ));
			$schedule_to->add(new DateInterval('PT30M'));

			echo $schedule->format('H:i')." - ".$schedule_to->format('H:i');
    }

    //stop editing
}


add_filter( "manage_edit-shop_order_sortable_columns", 'MY_COLUMNS_SORT_FUNCTION' );
function MY_COLUMNS_SORT_FUNCTION( $columns ) {


    $custom = array(
        //start editing
        'horaire'    => 'schedule'

        //stop editing
    );
    return wp_parse_args( $custom, $columns );
}



/* Message quand 5 commandes */
function action_woocommerce_new_order( $order_id ) { 
    $order = new WC_Order( $order_id );
    $myuser_id = (int)$order->user_id;

    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $myuser_id,
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
    ) );

    $numberOfOrder = count($customer_orders);

    global $current_user;
    get_currentuserinfo();

    if($numberOfOrder % 5 == 0)
    {
    	$title = "ChefBasil 5eme commande du client n°".$current_user->ID." ".$current_user->user_firstname." ".$current_user->user_lastname;
    	$message = "Le client n°".$current_user->ID." ".$current_user->user_firstname." ".$current_user->user_lastname." est à sa 5eme commande consécutive sur un total de ".$numberOfOrder;


    	$headers .='From: hello@chefbasil.fr' . "\r\n";
    	$headers .='Content-Type: text/plain; charset="utf-8"'." "; // ici on envoie le mail au format texte encodé en UTF-8
		$headers .='Content-Transfer-Encoding: 8bit'; // ici on précise qu'il y a des caractères accentués

		mail("contact@kevin-blanc.fr", $title, $message, $headers);
    	mail("humusbaby@gmail.com", $title, $message, $headers);
    }

    if($numberOfOrder == 1)
    {
    	$title = "ChefBasil 1ere commande du client n°".$current_user->ID." ".$current_user->user_firstname." ".$current_user->user_lastname;
    	$message = "Le client n°".$current_user->ID." ".$current_user->user_firstname." ".$current_user->user_lastname." est à sa 1ere commande";

    	$headers .='From: hello@chefbasil.fr' . "\r\n";
    	$headers .='Content-Type: text/plain; charset="utf-8"'." "; // ici on envoie le mail au format texte encodé en UTF-8
		$headers .='Content-Transfer-Encoding: 8bit'; // ici on précise qu'il y a des caractères accentués

		mail("contact@kevin-blanc.fr", $title, $message, $headers);
    	mail("humusbaby@gmail.com", $title, $message, $headers);
    }


}


add_action('wp_head', 'wpb_add_googleanalytics');
function wpb_add_googleanalytics() { ?>
	
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  <?php
  	 $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
  	 $tld = end(explode(".", parse_url($url, PHP_URL_HOST)));

  	 $idGA = "UA-92786039-1";

  	 if($tld == 'dev')
  	 {
  	 	$idGA = "UA-99326994-1";
  	 }
  ?>


  ga('create', '<?php echo $idGA; ?>', 'auto');
  ga('require', 'ecommerce');

</script>
<?php }

         
// add the action 
add_action( 'woocommerce_new_order', 'action_woocommerce_new_order', 10, 1 );


//Expiration du panier
/*add_filter('wc_session_expiring', 'filter_ExtendSessionExpiring' );
add_filter('wc_session_expiration' , 'filter_ExtendSessionExpired' );

function filter_ExtendSessionExpiring($seconds) {
    return 60;
}
function filter_ExtendSessionExpired($seconds) {
   return 120;
}*/

/**
 * Creating date collection between two dates
 *
 * <code>
 * <?php
 * # Example 1
 * date_range("2014-01-01", "2014-01-20", "+1 day", "m/d/Y");
 *
 * # Example 2. you can use even time
 * date_range("01:00:00", "23:00:00", "+1 hour", "H:i:s");
 * </code>
 *
 * @author Ali OYGUR <alioygur@gmail.com>
 * @param string since any date, time or datetime format
 * @param string until any date, time or datetime format
 * @param string step
 * @param string date of output format
 * @return array
 */
function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}