<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Vegan
 * @since Vegan 1.0
 */

$footer = apply_filters( 'vegan_get_footer_layout', 'default' );

?>


	</div><!-- .site-content -->
	
	
	<footer id="apus-footer" class="apus-footer" role="contentinfo">
		<?php if ( !empty($footer) ): ?>
			<?php vegan_display_footer_builder($footer); ?>
		<?php else: ?>
			
			<!--==============================powered=====================================-->
			
			<div class="apus-copyright">
				<div class="container">
					<div class="copyright-content">
						<div class="text-copyright pull-left">
						<?php
							if ( vegan_get_config('copyright_text') ) {
								echo esc_html(vegan_get_config('copyright_text'));
							} else {
								$allowed_html_array = array('strong' => array(),'a' => array('href'));
								echo wp_kses( __('Copyright &copy; 2016 - Vegan. All Rights Reserved. <br/> Powered by <a href="//apusthemes.com">ApusThemes</a>', 'vegan'), $allowed_html_array);
							}
						?>

						</div>
						<?php if ( has_nav_menu( 'footer-menu' ) ): ?>
		                <div class="pull-right">
		                    <nav class="apus-topmenu" role="navigation">
		                            <?php
		                                $args = array(
		                                    'theme_location'  => 'footer-menu',
		                                    'menu_class'      => 'menu',
		                                    'fallback_cb'     => '',
		                                    'menu_id'         => 'footer-menu'
		                                );
		                                wp_nav_menu($args);
		                            ?>
		                    </nav>                                                                     
		                </div>
		                <?php endif; ?>


						
					</div>
				</div>
			</div>
		
		<?php endif; ?>
		
	</footer><!-- .site-footer -->
	<?php
	if ( vegan_get_config('back_to_top') ) { ?>
		<a href="#" id="back-to-top">
			<i class="fa fa-angle-up"></i>
		</a>
	<?php
	}
	?>

</div><!-- .site -->
<div style='display: none;' id="modal_zone_de_livraison">
	<div id="map_zone_de_livraison" width='640' height='320'></div>
</div>


<?php wp_footer(); ?>
<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("0d6ca04399ef5910d6c2f210b5afc0d5");</script><!-- end Mixpanel -->
<?php if(!is_page(6)){ ?>
<!-- Chatra {literal} -->
<script>
	window.ChatraSetup = {
    buttonPosition: 'bl'
};

    (function(d, w, c) {
        w.ChatraID = 'Bn3mK27JeycZDy4rv';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
        + '//call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>


<!-- /Chatra {/literal} -->
	<?php } ?>
</body>
</html>