<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Vegan
 * @since Vegan 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<?php
 	global $post;	
	$fields = get_fields($post->ID);
?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width user-scalable=no">
	<meta name="description" content="<?php echo $fields['meta_title']; ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<LINK REL="SHORTCUT ICON" href="<?php echo site_url(); ?>/wp-content/themes/vegan/images/favicon.png">
	<?php wp_head(); ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
	<link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/vegan/modal/jquery.fancybox.min.css" type="text/css" media="screen" />
	<script src="<?php echo site_url(); ?>/wp-content/themes/vegan/modal/jquery.fancybox.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo site_url(); ?>/wp-content/themes/vegan/js/jquery.slidereveal.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLCUoyinfF3pIdiKJidvIhtITl1NU7sMk&libraries=places"></script>

	<script>


  
  ga('send', 'pageview');

	</script>

</head>
<body <?php body_class(); ?>>

<?php

	var_dump();
?>

<script type="text/javascript">
var baseUrl = '<?= site_url() ?>';
</script>
<?php

    $checkout_url = "";
    if( !is_user_logged_in() )
        {
            $checkout_url = "<a class='checkout_override_my_account my_account_link'>Paiement</a>"; 
        }
    else
        {   
            $checkout_url = "<a class='checkout_override'>Paiement</a>";
		}


	//Destroy item out of stock in cart
		global $woocommerce;
	foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
		
		$product = 	wc_get_product($cart_item['product_id']);

		$quantity = $cart_item['quantity'];


		if($quantity > $product->get_stock_quantity())
		{
			$woocommerce->cart->set_quantity($cart_item_key, 0);
		}
	}
?>


<div id="new_cart" class='new_cart_desktop'>
    <div class="mode-mini-cart"></div>
    <?php echo $checkout_url; ?>
</div>


<?php if ( vegan_get_config('preload') ) { ?>
	<div class="apus-page-loading">
	  	<div id="loader"></div>
	  	<div class="loader-section section-left"></div>
	  	<div class="loader-section section-right"></div>
	</div>
<?php } ?>
<div id="wrapper-container" class="wrapper-container">
<?php
	include dirname(__FILE__).'/inc/Mobile_Detect.php';
?>
	<?php get_template_part( 'headers/mobile/offcanvas-menu' ); ?>

	<?php get_template_part( 'headers/mobile/header-mobile' ); ?>

	<?php $header = apply_filters( 'vegan_get_header_layout', vegan_get_config('header_type') );
		if ( empty($header) ) {
			$header = 'v6';
		}
	?>
	<?php 
		get_template_part( 'headers/'.$header );
		$slug = get_post_field( 'post_name', get_post() );
	?>
	<div id="apus-main-content" class='page_<?php echo $slug; ?>'>