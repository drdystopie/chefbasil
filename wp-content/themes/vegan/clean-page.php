<?php
/**
 * Template Name: Clean Page
 * This template will only display the content you entered in the page editor
 */
?>
 
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body class='checkout-page'>

<?php

$class = "";

if(is_wc_endpoint_url( 'order-pay' ))
{
	$class = "order-pay";
}

?>

<div class="modal_checkout <?php echo $class; ?>">
<?php
    while ( have_posts() ) : the_post();   
        the_content();
    endwhile;
?>
</div>

<?php wp_footer(); ?>
</body>
</html>
