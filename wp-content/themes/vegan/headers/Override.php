<header id="apus-header" class="site-header header_override hidden-sm hidden-xs" role="banner">
    <div id="apus-topbar">
        <div class="container">
            <div class="topbar-inner clearfix">
                <div class="row">
                    <div class="col-md-5">
                    <?php if ( has_nav_menu( 'primary' ) ) : ?>

                                <?php
                                    $args = array(
                                        'theme_location' => 'primary',
                                        'container_class' => 'collapse navbar-collapse',
                                        'menu_class' => 'nav navbar-nav megamenu',
                                        'fallback_cb' => '',
                                        'menu_id' => 'primary-menu',
                                        'walker' => new Vegan_Nav_Menu()
                                    );
                                    wp_nav_menu($args);
                                ?>
                             
                    <?php endif; ?>
                    </div>
                    <div class="col-md-2" id="logo_header_desktop"><a href='<?php echo site_url(); ?>'></a></div>
                                        <div class="col-md-5 hidden-sm hidden-xs topbar-menu">
                       <?php if ( defined('VEGAN_WOOCOMMERCE_ACTIVED') && VEGAN_WOOCOMMERCE_ACTIVED ): ?>
                            <div class="pull-right">
                                <!-- Setting -->
                                <div class="top-cart">
                                    <?php 


                                        $countCart = WC()->cart->get_cart_contents_count();
                                        if($countCart == 0 || !is_front_page())
                                        {
                                            $styleNewCart = "display:none";
                                        }
                                    ?>
                                    <span id="icon_cart"></span>
                                    <span class="count_cart"><?php echo WC()->cart->get_cart_contents_count(); ?></span>


                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ( has_nav_menu( 'topmenu' ) ): ?>
                        <div class="pull-right wrapper-topmenu hidden-xs hidden-sm">
                            <nav class="apus-topmenu" role="navigation">
                                    <?php
                                        $args = array(
                                            'theme_location'  => 'topmenu',
                                            'menu_class'      => 'apus-menu-top list-inline',
                                            'fallback_cb'     => '',
                                            'menu_id'         => 'topmenu'
                                        );
                                        wp_nav_menu($args);
                                    ?>
                            </nav>                                                                     
                        </div>
                        <?php endif; 


                        global $current_user;
                        get_currentuserinfo();
                        ?>

                        <div class="user-login pull-right">
                            <ul class="list-inline">
                                <?php if( !is_user_logged_in() ){ ?>
                                    <li> <a class="my_account_link"> Connexion / Inscription </a></li>
                                <?php }else{ ?>
                                    <?php $current_user = wp_get_current_user(); ?>
                                  <li> <a id="my_welcome" class="hidden-xs" href="<?php echo site_url()."/mon-compte/orders" ?>"><?php echo $current_user->user_firstname; ?></a></span></li>
                                  <li><a id="my_logout" href="<?php echo wp_logout_url(home_url()); ?>"><?php esc_html_e('Logout ','vegan'); ?></a></li>
                                <?php } ?>
                            </ul>   
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</header>



<div id='woocommerce_my_account' class="mfp-hide"><?php echo do_shortcode("[woocommerce_my_account]"); ?></div>

<script type="text/javascript">
    

jQuery(document).ready(function(){
    <?php 
        global $woocommerce;
    ?>
    refresh_cart(<?php echo $woocommerce->cart->get_cart_contents_count(); ?>);
});
</script>