<?php
                                        $checkout_url = "";
                                        if( !is_user_logged_in() )
                                        {
                                            $checkout_url = "<a class='checkout_override_my_account my_account_link'>Paiement</a>"; 
                                        }
                                        else
                                        {   
                                            $checkout_url = "<a class='checkout_override'>Paiement</a>";
                                            
                                        }
    
    $detect = new Mobile_Detect;
    if ( $detect->isMobile() ) {
        ?>

        <?php
    }
?>

        <div id="slider">
            <div id="new_cart" class='new_cart_mobile'>
                <div class="mode-mini-cart"></div>
                <?php echo $checkout_url; ?>
            </div>
        </div>


<div id="apus-header-mobile" class="header-mobile hidden-lg hidden-md clearfix">
    <div class="container">
    <div class="row">
        <div class="col-xs-4 mobile_header_left">
            <div class="active-mobile pull-left">
                <button data-toggle="offcanvas" class="btn btn-sm btn-danger btn-offcanvas btn-toggle-canvas offcanvas" type="button">
                   <i class="fa fa-bars"></i>
                </button>
            </div>

        </div>
        <div id="logo_text_header" class="col-xs-4">
                <a href='<?php echo site_url(); ?>'></a>
        </div>
        <div class="col-xs-4 mobile_header_right">
                    
                    <button class="btn btn-sm btn-primary btn-outline dropdown-toggle pull-right"  type="button"><div class='count_cart trigger_right_menu'></div></button>

            <div class="active-mobile pull-right">
                <button  class="btn btn-sm btn-danger btn-toggle-canvas offcanvas trigger_right_menu" type="button">
                   <i class="fa fa-shopping-cart"></i>
                </button>
            </div>
                    <?php if( !is_user_logged_in() ){ ?>
                    <button class="btn btn-sm btn-primary btn-outline dropdown-toggle pull-right my_account_link"  type="button"><a class="fa fa-user"></a></button>
                    <?php }else{ ?>
                    <button class="btn btn-sm btn-primary btn-outline dropdown-toggle pull-right"  type="button"><a href='<?php echo site_url()."/mon-compte/orders" ?>' class="fa fa-user"></a></button>
                    <?php } ?>
            </div>
        </div>
    </div>
    </div>
</div>

<script>
jQuery(function(jQuery) {

    var slider = jQuery("#slider").slideReveal({
        trigger: jQuery(".trigger_right_menu"),
        position: "right",
          shown: function(slider, trigger){
                count_cart = parseInt(jQuery('.count_cart.trigger_right_menu').text());
                if(count_cart == 0)
                {
                    slider.slideReveal("hide");
                }
        }
      });


  jQuery('.trigger_right_menu').click(function(){
    jQuery('.closeMobileMenu').trigger( "click" );
  })
});
</script>