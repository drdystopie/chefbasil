<div id="apus-mobile-menu" class="apus-offcanvas hidden-lg hidden-md"> 
    <div class="apus-offcanvas-body">
        <div class="offcanvas-head bg-primary">
            <button type="button" class="btn btn-toggle-canvas btn-danger closeMobileMenu" data-toggle="offcanvas">
                <i class="fa fa-close"></i> 
            </button>
            <strong><?php esc_html_e( 'MENU', 'vegan' ); ?></strong>
        </div>

        <nav class="navbar navbar-offcanvas navbar-static" role="navigation">
            <div class="navbar-collapse navbar-offcanvas-collapse __web-inspector-hide-shortcut__">
                <ul id="main-mobile-menu" class="nav navbar-nav">
                    <li id="menu-item-2158" class="menu-item-2158"><a href="<?php echo site_url(); ?>/notre-concept/">Notre concept</a></li>
                    
                    <li id="menu-item-2158" class="menu-item-2158"><a class="trigger_right_menu">Mon panier</a></li>

                    <?php if( !is_user_logged_in() ){ ?>
                    <li id="menu-item-2158" class="menu-item-2158 my_account_link"><a>Connexion</a></li>
                    <?php }else{ ?>
                    <li id="menu-item-2158"><a id="my_welcome"  href="<?php echo site_url()."/mon-compte/orders" ?>">Mon compte</a></li>
                    <li><a id="my_logout" href="<?php echo wp_logout_url(home_url()); ?>"><?php esc_html_e('Logout ','vegan'); ?></a></li>
                    <?php } ?>

                </ul>
            </div>
        </nav>

    </div>
</div>