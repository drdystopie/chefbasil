<?php

/**
 * Plugin Name: WooCommerce Sponsor a Friend Plugin
 * Description: Allow your customers to sponsor a friend and get rewards for them and their friends.
 * Version: 2.3.3
 * Text Domain: wsafp
 * Domain Path: /languages/
 * Author: MB Création
 * Author URI: http://www.mbcreation.net
 * License: http://codecanyon.net/licenses/regular_extended
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required Classes

include( dirname(__FILE__).'/class.setup.php');
include( dirname(__FILE__).'/class.back.php');
include( dirname(__FILE__).'/class.front.php');
include( dirname(__FILE__).'/class.order.php');
include( dirname(__FILE__).'/class.request.php');

// Activation
register_activation_hook( __FILE__, array( 'WooCommerce_Sponsor_a_Friend_Plugin', 'install' ) );

// Uninstall
register_uninstall_hook( __FILE__, array( 'WooCommerce_Sponsor_a_Friend_Plugin', 'uninstall' ) );


// Early Hook
add_action('woocommerce_coupon_loaded', array('WooCommerce_Sponsor_a_Friend_Plugin_Front', 'coupon_type' ) );
add_filter( 'plugin_action_links_'.plugin_basename( __FILE__ ), array('WooCommerce_Sponsor_a_Friend_Plugin_Back','action_links'), 10, 2 );

// Loader
add_action( 'plugins_loaded' , 'WooCommerce_Sponsor_a_Friend_Plugin_Loader');



function WooCommerce_Sponsor_a_Friend_Plugin_Loader(){

	load_plugin_textdomain('wsafp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');

	if(class_exists('Woocommerce')){


		if( is_admin() && current_user_can('manage_options') ){

			// updating option.
			$options_update = new WooCommerce_Sponsor_a_Friend_Plugin();

			// loading backend.
			$GLOBALS['WooCommerce_Sponsor_a_Friend_Plugin_Back'] = new WooCommerce_Sponsor_a_Friend_Plugin_Back();

		}
		else{
		
			$GLOBALS['WooCommerce_Sponsor_a_Friend_Plugin_Front'] = new WooCommerce_Sponsor_a_Friend_Plugin_Front();
		
		}
		

	}
	
}


