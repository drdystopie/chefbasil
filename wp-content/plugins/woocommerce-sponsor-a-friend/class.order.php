<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce_Sponsor_a_Friend_Plugin_Order
 * Extends WC_order
 * @since 1.0
 */

if ( ! class_exists( 'WooCommerce_Sponsor_a_Friend_Plugin_Order' ) ) {


	class WooCommerce_Sponsor_a_Friend_Plugin_Order {

		private $wsaf_coupons;
		public $wc_order;
		public $parrain;
		public $sponsorfirstname;
		public $order_id;

		function __construct($order_id){

				$this->order_id = $order_id;
				$this->wc_order = new WC_Order($order_id);

		} // __construct

		public function has_coupon() {
	        	
	        	$this->wsaf_coupons = $this->wc_order->get_items( array( 'coupon' ) );
	        	return !empty($this->wsaf_coupons);
		
		} // has_coupon

		public function has_sponsor_coupon() {

				global $wpdb;

				if(empty($this->wsaf_coupons))
					return false;

				foreach ( $this->wsaf_coupons as $item_id => $item ) {


					$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM {$wpdb->posts} WHERE post_title = %s AND post_type = 'shop_coupon' AND post_status = 'publish' LIMIT 1;", $item['name'] ) );
					$this->parrain = get_post_meta($post_id,'wsafp_sponsor_user_id', true);
					$this->sponsorfirstname = get_post_meta( $post_id, 'wsafp_sponsor_firstname',  true );

				}

				return !empty($this->parrain);

		} // has_sponsor_coupon


		public function sponsor_email(){

				$user = get_user_by( 'id', $this->wsaf_coupons );
				return $user->user_email;

		} // sponsor_email

		public function update_status(){

				update_post_meta( $this->order_id, 'wsafp_coupon_issued', '1' );

		}

		public function has_been_check_for_coupon(){

				$meta = get_post_meta( $this->order_id , 'wsafp_coupon_issued' , true );
				return !empty($meta);
		
		}


	} // class order

} // class_exists
