<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce_Sponsor_a_Friend_Plugin_Front
 * Class for frontend.
 * Displays a form to customers only so they can recommend the store to a friend
 * 
 * @since 1.0
 */


if ( ! class_exists( 'WooCommerce_Sponsor_a_Friend_Plugin_Front' ) ) {

class WooCommerce_Sponsor_a_Friend_Plugin_Front{
		
		private $options = array();
		private $form_action;

		function __construct()
		{	

			$this->hooks();
			$this->options = get_option('wsafp_options');
		
		} // __construct


		public function hooks()
		{

			add_action('init', array( &$this, 'form_treatment') );
			add_filter('body_class',array( &$this, 'wsafp_body_class') );
			add_shortcode('wsafp-form-shortcode', array( &$this, 'form_shortcode' ) );
			add_action('woocommerce_thankyou', array($this,'woocommerce_thankyou') );
			add_action('woocommerce_order_status_completed', array('WooCommerce_Sponsor_a_Friend_Plugin_Front','check_for_sponsor') );

		

		} // hooks

		public function wsafp_body_class($classes)
		{

			/**
	        * WPML Support
	        */

            if( defined( 'ICL_LANGUAGE_CODE' ) ) {

            	if( !empty($this->options['wsafp_page']) && is_page( icl_object_id($this->options['wsafp_page'],'page') ) ) :

	                $classes[] = 'woocommerce';
					$classes[] = 'woocommerce-page';
					$classes[] = 'woocommerce-sponsor-a-friend-page';

				endif;

            }
            elseif ( !empty($this->options['wsafp_page']) && is_page( $this->options['wsafp_page'] ) ) {
            	$classes[] = 'woocommerce';
				$classes[] = 'woocommerce-page';
				$classes[] = 'woocommerce-sponsor-a-friend-page';
            }

			if( is_order_received_page() && $this->options['wsafp_display_on_thankyou_page'] === 'yes' )
				$classes[] = 'woocommerce-sponsor-a-friend-page';

			return $classes;
		
		} // wsafp_body_class

		public function woocommerce_thankyou($order_id){

			if( $this->options['wsafp_display_on_thankyou_page'] !=='yes' )
				return;

			$form = $this->options['wsafp_before_thankyou_form'];
			$form.= do_shortcode( '[wsafp-form-shortcode oid="'.$order_id.'"]' );

			echo apply_filters('wsafp_thankyou_form_filter', $form );
		}

		public function form_treatment(){

			if($_SERVER['REQUEST_METHOD']!=='POST')
			return;

			if(!isset($_POST['wsafp']))
			return;

			if(!wp_verify_nonce( $_REQUEST['_wpnonce'], 'wsafp-sponsor_'.$_SERVER['REMOTE_ADDR'] ))
			return;

			$requestdata = array_map('sanitize_text_field',$_POST['wsafp']);

			$request = new WooCommerce_Sponsor_a_Friend_Plugin_Request();

			$request->setType('front');

			$user_ID = is_user_logged_in() ? get_current_user_id() : $requestdata['gemail'] ;

			$request->populate($user_ID, $requestdata);

			if($request->isValid()){

				global $wpdb;
				$result = $wpdb->query("INSERT INTO `wp_aff_limit` (`id`, `id_user`) VALUES (NULL, '".$user_ID."')");

				$request->generateCodePromo();
				$request->sendEmail();
				$request->success();



			}

			else{

				$request->display_error();

			}


			

		}

		public function form_shortcode($atts){


			global $woocommerce;

			// redirect success message

			if(isset($_GET['wsaf-status']) && $_GET['wsaf-status'] == 'success')
			{
				
				$successMessage = apply_filters('wsafp_form_shorcode_congratmessage_filter', __('Congratulations, your message was sent to your friend !','wsafp') );

				if( function_exists('wc_add_notice') ) {
					wc_add_notice($successMessage,'success');
				}
				else{
					$woocommerce->add_message($successMessage);
				}
			}

			if( function_exists('wc_print_notices') ){
				wc_print_notices();
			}
			else{
				$woocommerce->show_messages();
			}
		   	
		   	
		   	$gform = '';

	
			$gfirstnameLabel = apply_filters( 'wsafp_form_shorcode_gfirstnamelabel_filter' , __('Your first name','wsafp') );
			$gnameLabel = apply_filters( 'wsafp_form_shorcode_glastnamelabel_filter' , __('Your last name','wsafp') );
			$gemailLabel = apply_filters( 'wsafp_form_shorcode_gemaillabel_filter' , __('Your email','wsafp') );
			$gformDisplay = "display:block;";		
			
			if( is_user_logged_in() ) {

				$current_user=wp_get_current_user();
				$gEmail = (empty($_POST['wsafp']['gemail'])) ? $current_user->user_email : esc_attr( $_POST['wsafp']['gemail'] );
				$gFirstname = (empty($_POST['wsafp']['gfirstname'])) ? $current_user->user_firstname : esc_attr( $_POST['wsafp']['gfirstname'] );
				$gName = (empty($_POST['wsafp']['gname'])) ? $current_user->user_lastname : esc_attr( $_POST['wsafp']['gname'] );
				
				if( $gName !== '' && $gFirstname !== '' && $gEmail !== '' ) 
				$gformDisplay = "display:none;";

			}
				
			if( is_order_received_page() && isset($atts['oid']) ) {

				$gEmail = get_post_meta( $atts['oid'],'_billing_email', true );
				$gFirstname = get_post_meta( $atts['oid'],'_billing_first_name', true  );
				$gName = get_post_meta( $atts['oid'],'_billing_last_name', true  );

				if( $gName !== '' && $gFirstname !== '' && $gEmail !== '' ) 
				$gformDisplay = "display:none;";

			}

			if( !is_user_logged_in() && !is_order_received_page() ){

				$gEmail = (empty($_POST['wsafp']['gemail'])) ? '' : esc_attr( $_POST['wsafp']['gemail'] );
				$gFirstname = (empty($_POST['wsafp']['gfirstname'])) ? '' : esc_attr( $_POST['wsafp']['gfirstname'] );
				$gName = (empty($_POST['wsafp']['gname'])) ? '' : esc_attr( $_POST['wsafp']['gname'] );
				$gformDisplay = "display:block;";

			}

			$gform = <<<HTML
			<div style="$gformDisplay">
			<p class="form-row form-row-first form-row-gfirstname">
				<label for="wsafp-gfirstname">$gfirstnameLabel<span class="required">*</span></label>
				<input value="$gFirstname" type="text" required class="input-text" name="wsafp[gfirstname]" id="wsafp-gfirstname">
			</p>

			<p class="form-row form-row-last form-row-glastname">
				<label for="wsafp-gname">$gnameLabel<span class="required">*</span></label>
				<input value="$gName" type="text" required class="input-text" name="wsafp[gname]" id="wsafp-gname">
			</p>
			
			<div class="clear"></div>

			<p class="form-row form-row-first form-row-gemail">
				<label for="wsafp-gemail">$gemailLabel<span class="required">*</span></label>
				<input value="$gEmail" type="text" required class="input-text" name="wsafp[gemail]" id="wsafp-gemail">
			</p>
			
			<div class="clear"></div>
			</div>

HTML;

			
				
			$firstnameLabel = apply_filters( 'wsafp_form_shorcode_firstnamelabel_filter' , __('Your friend\'s first name','wsafp') );
			$nameLabel = apply_filters( 'wsafp_form_shorcode_lastnamelabel_filter' , __('Your friend\'s last name','wsafp') );
			$emailLabel = apply_filters( 'wsafp_form_shorcode_emaillabel_filter' , __('Your friend\'s email','wsafp') );
			$buttonLabel = apply_filters( 'wsafp_form_shorcode_buttonlabel_filter' , __('Refer a friend','wsafp') );
			$textareaLabel = apply_filters( 'wsafp_form_shorcode_textarealabel_filter' , __('Optional message','wsafp') );
			$textareaPlaceholder = apply_filters( 'wsafp_form_shorcode_textareaplaceholder_filter' , __('Will be included in the invitation email','wsafp') );

			$filleulEmail = (empty($_POST['wsafp']['email'])) ? '' : esc_attr( $_POST['wsafp']['email'] );
			$filleulFirstname = (empty($_POST['wsafp']['firstname'])) ? '' : esc_attr( $_POST['wsafp']['firstname'] );
			$filleulName = (empty($_POST['wsafp']['name'])) ? '' : esc_attr( $_POST['wsafp']['name'] );
			$textareaValue = (empty($_POST['wsafp']['message'])) ? '' : esc_textarea( $_POST['wsafp']['message'] );
			$this->form_action = get_permalink( intval($this->options['wsafp_page']) );

			if( defined( 'ICL_LANGUAGE_CODE' ) && function_exists('icl_object_id') )
				$this->form_action = get_permalink(icl_object_id($this->options['wsafp_page'],'page'));


			$user_ID = get_current_user_id();
			global $wpdb;
			$nbParrainage = $wpdb->get_row('SELECT count(*) as nbParrainage from wp_aff_limit where id_user = '.$user_ID);
			if($nbParrainage->nbParrainage < 20)
			{
				$form = <<<HTML

				<form method="post" class="wsafp-form" action="{$this->form_action}">
					$gform
					<p class="form-row form-row-first form-row-firstname">
						<label for="wsafp-firstname">$firstnameLabel<span class="required">*</span></label>
						<input value="$filleulFirstname" type="text" required class="input-text" name="wsafp[firstname]" id="wsafp-firstname">
					</p>

					<p class="form-row form-row-last form-row-lastname">
						<label for="wsafp-name">$nameLabel<span class="required">*</span></label>
						<input value="$filleulName" type="text" required class="input-text" name="wsafp[name]" id="wsafp-name">
					</p>

					<div class="clear"></div>

					<p class="form-row form-row-first form-row-email">
						<label for="wsafp-email">$emailLabel<span class="required">*</span></label>
						<input value="$filleulEmail" type="email" required class="input-text" name="wsafp[email]" id="wsafp-email">
					</p>

					<p class="form-row form-row-last form-row-message">
					<label for="wsafp-message" class="">$textareaLabel</label>
					<textarea name="wsafp[message]" class="textarea" id="wsafp-message" placeholder="$textareaPlaceholder" cols="20" rows="4">$textareaValue</textarea>
					</p>
					
					<div class="clear"></div>

					<p class="form-row form-row-submit">
						<input type="submit" class="button" name="wsafp[sponsor]" value="$buttonLabel">
					</p>

HTML;
				$form.= wp_nonce_field( 'wsafp-sponsor_'.$_SERVER['REMOTE_ADDR'] , '_wpnonce', true, false );
				$form.='</form>';
			}
			else
			{
				return "<div style='font-size: 1.3vw;color: #E05758;'>Vous avez atteint le quota des 20 parrainages. Notre campagne de parrainage s'arrête automatiquement une fois ce cap passé.<br>
						Tout le monde rêverait d'avoir un parrain comme vous. Merci mille fois !</div>";
			}

			

		
			return apply_filters( 'wsafp_form_shorcode_html_filter' , $form );

		} // form_shortcode

		public static function coupon_type($coupon)
		{		

				if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        			return;


				if( version_compare( WC_VERSION, '2.7', '<' ) ){
			
					
					if($coupon->type == 'wsafp_coupon' or $coupon->type == 'wsafp_reward_coupon'){

						// backward comptatibily
						$coupon->type = 'fixed_cart';
						$coupon->discount_type = 'fixed_cart';

							if( isset($coupon->coupon_custom_fields['wsafp_coupon_type_alias'])
							& !empty($coupon->coupon_custom_fields['wsafp_coupon_type_alias'][0]) ) {
								
								$coupon->type = $coupon->coupon_custom_fields['wsafp_coupon_type_alias'][0];
								$coupon->discount_type = $coupon->coupon_custom_fields['wsafp_coupon_type_alias'][0];
							
							}
						
					}

				}else{

					if( $coupon->get_meta('wsafp_coupon_type_alias') ){		
						
							$coupon->set_discount_type( $coupon->get_meta('wsafp_coupon_type_alias') );

					}

				}
		
				return $coupon;

		} // handle_coupon



		/**
		 * check_for_sponsor
		 * On order completed. Check if a sponsor coupon was used.
		 * @since 1.0
		 */

		public static function check_for_sponsor($order_id)
		{

		
			$order = new WooCommerce_Sponsor_a_Friend_Plugin_Order($order_id);

			

			if ( $order->wc_order->has_status( 'processing' ) && get_option( 'woocommerce_downloads_grant_access_after_payment' ) == 'no' ) {
				return;
			}

			if($order->has_coupon() && $order->has_sponsor_coupon() && !$order->has_been_check_for_coupon()) {			


				$request = new WooCommerce_Sponsor_a_Friend_Plugin_Request();

				$request->setType('back');

				if( is_email($order->parrain) ) :

					$request->parrain = $order->parrain;
					$request->parrainEmail = $order->parrain;
					$request->parrainShortName = $order->sponsorfirstname;
	
				else :

					$request->parrain = $order->parrain;
					$request->parrainEmail = $request->getParrainEmail();
					$request->parrainShortName = $request->getParrainShortname();
					
				endif;


				if( version_compare( WC_VERSION, '2.7', '<' ) ){
					$request->setFirstName( $order->wc_order->billing_first_name );
					$request->setFilleul($order->wc_order->billing_first_name.' '.$order->wc_order->billing_last_name);
				}
				else{
					$request->setFirstName( $order->wc_order->get_billing_first_name() );
					$request->setFilleul( $order->wc_order->get_billing_first_name().' '.$order->wc_order->get_billing_last_name() );
				}
				// Is request eligible to coupon accumulation ?

				if($GLOBALS['WooCommerce_Sponsor_a_Friend_Plugin_Front']->options['wsafp_coupon_allow_aggregate']=='yes' && $request->isUpdatable()){

					$request->updateCodePromo();

				}

				else{

					$request->generateCodePromo();

				}

				$request->sendEmail();


				// Make sure coupon is issued just once
				$order->update_status();
			
			}

		} // check_for_sponsor

	} // WooCommerce_Sponsor_a_Friend_Plugin_Front
}


