<?php
/**
 * WooCommerce_Sponsor_a_Friend_Plugin
 * For install & uninstall
 * 
 * @since 1.0
 */

if ( ! class_exists( 'WooCommerce_Sponsor_a_Friend_Plugin' ) ) {

class WooCommerce_Sponsor_a_Friend_Plugin{
		
		public static $options_name = 'wsafp_options';
		public static $default_options  = array(

			// Allow none customer to refer friend
			'wsafp_allow_none_customer' => 'no',

			// No page at install
			'wsafp_page' => '',

			// No text at install
			'wsafp_before_thankyou_form' => '',

			// Default fixed_cart type
			'wsafp_coupon_type_alias' => 'fixed_cart',

			// Coupon default Amount.
			'wsafp_coupon_amount' => 10,

			// Individual Use
			// @since 1.2
			'wsafp_coupon_individual_use' => 'yes',

			// Coupon default Duration (in days).
			'wsafp_coupon_duration' => 45,

			// Coupon minimum Amount.
			'wsafp_minimum_amount' => 0,

			// Email to friend content
			'wsafp_email_to_friend_content' => "
			Hi {{friend}},

			{{sponsor}} thinks you should be shopping with {{sitename}} !
			Message from {{sponsor}} :

			{{message}}

			As welcome gift, here's a {{amount}} voucher you can use to discover our great products.

			Personnel gift code : <strong>{{coupon}}</strong>
			Email address associated with it *: <strong>{{email}}</strong>
			This coupon is valid until : <strong>{{validity}}</strong>
			
			See you soon on {{homeurl}} !

			* You will need to use this email as the billing email address on checkout.",

			'wsafp_email_to_friend_subject' => '{{sponsor}} thinks you should be shopping at {{sitename}}',
			
			'wsafp_email_to_friend_heading' => '{{sponsor}} thinks you should be shopping at {{sitename}}',



			// Default fixed_cart type
			'wsafp_coupon_type_alias_sponsor' => 'fixed_cart',

			// Coupon default Amount.
			'wsafp_coupon_amount_sponsor' => 10,

			// Individual Use
			// @since 1.2
			'wsafp_coupon_individual_use_sponsor' => 'yes',

			// Apply before tax
			// @since 1.2
			'wsafp_coupon_apply_before_tax_sponsor' => 'no',

			// Coupon default Duration (in days).
			'wsafp_coupon_duration_sponsor' => 45,

			// Coupon minimum Amount.
			'wsafp_minimum_amount_sponsor' => 0,

			// Sponsor can aggregate coupon
			// @since 1.2
			'wsafp_coupon_allow_aggregate' => 'no',

			'wsafp_display_on_thankyou_page' => 'no',

			'wsafp_email_to_sponsor_content' => "

			Hi {{sponsor}},
			
			{{friend}} just ordered on our site thanks to you !
			
			As reward gift, here's a {{amount}} voucher you can use to order great new products.

			Personnel gift code : <strong>{{coupon}}</strong>
			Email address associated with it *: <strong>{{email}}</strong>
			This coupon is valid until : <strong>{{validity}}</strong>

			See you soon on {{homeurl}} !

			* You will need to use this email as the billing email address on checkout.",

			'wsafp_email_to_sponsor_subject' => '{{friend}} just ordered on {{sitename}} thanks to you !',

			'wsafp_email_to_sponsor_heading' => '{{friend}} just ordered on {{sitename}} thanks to you !'
		
		);

		
		function __construct()
		{	
			
			self::update_option();

		} // __construct


		public function init(){

			


		}
	
		public static function install()
		{


			if ( ! current_user_can( 'activate_plugins' ) )
            return;

        	$current_options = wp_parse_args( get_option(self::$options_name), self::$default_options );
			
        	update_option( self::$options_name , $current_options );

					
		} // install



		public static function uninstall()
		{

			

		
		} // uninstall

		public static function update_option(){

			$current_options = wp_parse_args( get_option(self::$options_name), self::$default_options );
			update_option( self::$options_name , $current_options );

		}


	} // WooCommerce_Sponsor_a_Friend_Plugin
}