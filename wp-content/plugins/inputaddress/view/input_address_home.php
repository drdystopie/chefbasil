<div id="inputAdressHome">
	<div id="inputAdressHome_p1">
		<div id="velo_decoration_container">
			<div id="velo_decoration"></div>
			<img class='triangle_img_left' src='<?php echo site_url();?>/wp-content/themes/vegan/images/triangle_gauche.png'>
		</div>
		<?php
				$now = (new \DateTime());
				$now->sub(new DateInterval('PT15M'));
		?>

		<div class='debug' style='display: none;'><?php var_dump($now); ?></div>
		

		<input type='text' id="searchAddress" placeholder="Saisissez votre adresse de livraison ici"></input>
	</div>
	<div id="inputAdressHome_p2">

	<div id="horloge_decoration_container"><div id="horloge_decoration"></div></div>

	<div id='container_select'>

	<select id='schedule_address'>
		<option value="" disabled selected>A quelle heure ?</option>
		  <?php

		  global $wpdb;
		  $option = $wpdb->get_row("SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend' LIMIT 1");
		  $option = $option->option_value;

		  	foreach ($schedules['matin'] as $key => $schedule) {


		  			$dayNumber = date('w');

		  			$disabled = "";
		  			
		  			if(!current_user_can('administrator'))
		  			{
		  				if($now > $schedule['date_from'] || $dayNumber == 6 || $dayNumber == 0 || $option == "1" || $schedule['desactivate'])
		  				{
		  					$disabled = "disabled";
		  				}
		  			}


		  			echo "<option ".$disabled.	" value='".$schedule['date_from']->getTimestamp()."'>De ".$schedule['date_from']->format('H:i')." à ".$schedule['date_to']->format('H:i')."</option>";
		  	}

		  	echo "<option value='' disabled>----------</option>";

		  	foreach ($schedules['soir'] as $key => $schedule) {

		  			$dayNumber = date('w');

		  			$disabled = "";

		  			$option = (get_option( 'modal_weekend' ));

		  			if(!current_user_can('administrator'))
		  			{
		  				if($now > $schedule['date_from'] || $dayNumber == 6 || $dayNumber == 0 || $option == "1" || $schedule['desactivate'])
		  				{
		  				$disabled = "disabled";
		  				}
		  			}


		  			echo "<option ".$disabled." value='".$schedule['date_from']->getTimestamp()."'>De ".$schedule['date_from']->format('H:i')." à ".$schedule['date_to']->format('H:i')."</option>";
		  	}
		  ?>
	</select>
	</div>

	<input type='hidden' id='postalCodeHidden'>
</div>

<script>

	jQuery( document ).ready(function() {
		if(jQuery.cookie('chefbasil_addr') != null)
		{
			jQuery('#searchAddress').val(jQuery.cookie('chefbasil_addr_billing_lineOne')+", "+jQuery.cookie('chefbasil_addr_billing_city')+", "+"France");
		}

		if(jQuery.cookie('chefbasil_schedule') != null)
		{
			chefbasil_schedule_cookie = jQuery.cookie('chefbasil_schedule');
			var found = false;

			jQuery("#schedule_address option").each(function () {
			   if(jQuery(this).val() == chefbasil_schedule_cookie && jQuery(this).is(':enabled'))
			   {
			   	jQuery('#schedule_address').val(chefbasil_schedule_cookie)
			   	found = true;
			   }
			});

			if(!found)
			{
				jQuery.removeCookie('chefbasil_schedule');
			}

		}

	});


	jQuery( "#searchAddress" ).keyup(function() {
		chefbasil_addr = jQuery(this).val();
		if( chefbasil_addr == "")
		{
			jQuery.removeCookie("chefbasil_addr");
		}
		
	});

	jQuery( "#schedule_address" ).change(function() {
		jQuery.cookie('chefbasil_schedule', jQuery('#schedule_address').val());
	});

	var autocomplete;

	function initialize() {

		var input = document.getElementById('searchAddress');
		var option = {componentRestrictions: {country: 'fr'}};
		autocomplete = new google.maps.places.Autocomplete(input,option);
		autocomplete.addListener('place_changed', getPostalCode);
	}

	function getPostalCode() {
		//jQuery( "#schedule_address" ).prop('selectedIndex',0);
		var place = autocomplete.getPlace();

		var placeParsed = placeToAddress(place);

		console.log(typeof placeParsed.StreetNumber === 'undefined');


		if(typeof placeParsed.Zip !== 'undefined' && typeof placeParsed.StreetNumber !== 'undefined' && typeof placeParsed.StreetName !== 'undefined' && typeof placeParsed.City !== 'undefined')
		{
			var postalCode = placeParsed.Zip.long_name;

			var lineOne = placeParsed.StreetNumber.long_name+" "+placeParsed.StreetName.long_name

			var city = placeParsed.City.long_name;

			jQuery.ajax({
				url: '<?= admin_url( 'admin-ajax.php' ); ?>', 
				type: 'get',
				data: {
					'action': 'address_check',
					'postalCode': postalCode
				}, 
				success: function(response) {
					jQuery("#postalCodeHidden").val(response);
					if(response == "true")
					{
						jQuery.cookie('chefbasil_addr', jQuery('#searchAddress').val());
						jQuery.cookie('chefbasil_addr_billing_lineOne', lineOne);
						jQuery.cookie('chefbasil_addr_billing_city', city);
						jQuery.cookie('chefbasil_addr_billing_postalCode', postalCode);
					}
					else
					{
						jQuery('#searchAddress').val("");
						jQuery.fancybox.open({  src  : '#modal_pas_de_livraison_dans_cette_zone',closeBtn   : false});
					}
				
				}
			});
		}
		else
		{
			jQuery('#searchAddress').val("");
			jQuery.fancybox.open({  src  : '#modal_zone_not_found',closeBtn   : false});
		}


	}

	google.maps.event.addDomListener(window, 'load', initialize);


	function placeToAddress(place){
        var address = {};
        place.address_components.forEach(function(c) {
            switch(c.types[0]){
                case 'street_number':
                    address.StreetNumber = c;
                    break;
                case 'route':
                    address.StreetName = c;
                    break;
                case 'neighborhood': case 'locality':    // North Hollywood or Los Angeles?
                    address.City = c;
                    break;
                case 'administrative_area_level_1':     //  Note some countries don't have states
                    address.State = c;
                    break;
                case 'postal_code':
                    address.Zip = c;
                    break;
                case 'country':
                    address.Country = c;
                    break;
                /*
                *   . . . 
                */
            }
        });

        return address;
    }


</script>
</div>

<div style="display: none;" id="modal_add_quantity_addr">
	<img src='<?php echo get_stylesheet_directory_uri() ?>/images/modal_add_quantity_addr.png'>
</div>


<div style="display: none;" id="modal_add_quantity_schedule">
	<img src='<?php echo get_stylesheet_directory_uri() ?>/images/modal_add_quantity_schedule.png'>
</div>

<div style="display: none;" id="modal_pas_de_livraison_dans_cette_zone">
	<div class='title'>PAS DE LIVRAISON DANS CETTE ZONE</div>
	<div class='content'>Malheureusement, nous ne livrons pas encore par ici mais nous faisons au mieux pour y remédier ! A qui le tour ? Votez pour votre arrondissement pour être le prochain : <br><a class='vote_zone'>9</a> - <a class='vote_zone'>10</a> - <a class='vote_zone'>11</a> - <a class='vote_zone'>12</a> - <a class='vote_zone'>13</a> - <a class='vote_zone'>14</a> - <a class='vote_zone'>15</a> - <a class='vote_zone'>16</a> </div>
</div>

<div style="display: none;" id="modal_pas_de_livraison_dans_cette_zone_merci">
	<div class="title">Merci ! </div>
	<div class="content">Votre vote a été pris en compte</div>
</div>

<div style="display: none;" id="modal_zone_not_found">
	<div class="title">Oups !</div>
	<div class="content">Oups, on ne vous trouve pas. Indiquez-nous une adresse plus précise et on arrive !</div>
</div>

<script type="text/javascript">
	

jQuery('.vote_zone').click(function() {
  	zone = jQuery(this).text();
  	console.log(zone);

  	jQuery.post(
    woocommerce_params.ajax_url,
    {
        'action': 'add_vote',
        'param': zone
    },
    function(response){
            jQuery.fancybox.close({  src  : '#modal_pas_de_livraison_dans_cette_zone'});
            jQuery.fancybox.open({  src  : '#modal_pas_de_livraison_dans_cette_zone_merci',closeBtn   : false});
        }
	);

});




</script>


