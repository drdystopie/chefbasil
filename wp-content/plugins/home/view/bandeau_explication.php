<div id="bandeau_explication_home">
	<div class="row">
			<div class="bandeau_explication_home_element margin-left">
				<div class="bandeau_explication_home_element_img">
					<img src="<?php echo site_url() ?>/wp-content/themes/vegan/images/explication_maison.png" />
				</div>
				<div class="bandeau_explication_home_element_text">
					<div class="bandeau_explication_home_titre">Fait maison par notre chef olivier</div> 
					<div class="bandeau_explication_home_sous_titre">Et ben non il ne s’appelle pas Basil</div>
				</div>
				
				<?php if ( is_page(2152)){ ?>
				<div class='bandeau_explication_bottom'>
					Notre brigade s’affaire tous les matins à vous cuisiner de bons plats à partir de produits frais, de saison, produits au plus près de chez nous !
				</div>
				<?php } ?>
			</div>

			<div class="bandeau_explication_home_element">
				<div class="bandeau_explication_home_element_img">
					<img src="<?php echo site_url() ?>/wp-content/themes/vegan/images/explication_geoposition.png" />
				</div>
				<div class="bandeau_explication_home_element_text">
					<div class="bandeau_explication_home_titre">Livré à marseille en vélo </div> 
					<div class="bandeau_explication_home_sous_titre">Ou en scooter si vous êtes aux Goudes, du 1er au 8 ème arrondissement.</div>
				</div>
				
				<?php if ( is_page(2152)){ ?>
				<div class='bandeau_explication_bottom'>
					Renseignez une adresse, un créneau de livraison, nos BB - Basil Bikers - prendront la suite des opérations pour vous retrouver au plus vite à la porte de votre maison, bâtiment ou résidence dans les 8 premiers arrondissements de Marseille.
				</div>
				<?php } ?>
			</div>

			<div class="bandeau_explication_home_element">
				<div class="bandeau_explication_home_element_img">
					<img src="<?php echo site_url() ?>/wp-content/themes/vegan/images/explication_horloge.png" />
				</div>
				<div class="bandeau_explication_home_element_text">
					<div class="bandeau_explication_home_titre">11h30 - 14h30<br> 19h00 - 22h30<br> Lundi au Vendredi </div> 
					<div class="bandeau_explication_home_sous_titre"><?php if ( !is_page(2152)){ ?>Faites-nous savoir si nous vous manquons le week-end !<?php } ?></div>
				</div>
				
				<?php if ( is_page(2152)){ ?>
				<div class='bandeau_explication_bottom'>
					Nos BB vous livrent toute la semaine, midi et soir, sur des créneaux élargis pour s’adapter au mieux à votre emploi du temps !
				</div>
				<?php } ?>
			</div>

			<div class="bandeau_explication_home_element margin-right">
				<div class="bandeau_explication_home_element_img">
					<img src="<?php echo site_url() ?>/wp-content/themes/vegan/images/explication_couverts.png" />
				</div>
				<div class="bandeau_explication_home_element_text">
					<div class="bandeau_explication_home_titre">Il ne vous reste plus qu’à réchauffer si besoin et déguster</div> 
					<div class="bandeau_explication_home_sous_titre">& recycler à la fin !</div>
				</div>

				<?php if ( is_page(2152)){ ?>
				<div class='bandeau_explication_bottom'>
					Parce que votre satisfaction est notre priorité, votre commande est livrée froide afin de préserver toutes les saveurs et vous laisser le choix de déguster quand vous le souhaitez. Vous pourrez ainsi vous faire livrer votre dîner en même temps que votre déjeuner ou commander avant d’entrer en réunion pour déguster en sortant !
				</div>
				<?php } ?>
			</div>
	</div>
</div>