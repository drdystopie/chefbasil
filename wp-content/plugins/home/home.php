<?php
/*
Plugin Name: Home
Description: Gestion de la home
Version: 1.0
*/

function bandeau_explication(){

	$schedules = getAllPossibleSchedule();
	include(plugin_dir_path( __FILE__ )."view/bandeau_explication.php");
}

add_shortcode('bandeau_explication', 'bandeau_explication');



add_action( 'wp_ajax_add_vote', 'add_vote' );
add_action( 'wp_ajax_nopriv_add_vote', 'add_vote');

function add_vote() {

	$param = $_POST['param'];

	echo $param;

	global $wpdb;
		$wpdb->insert( 
		'vote_zones', 
		array( 
			'zone_id' =>$param, 
		)
	);
	die();
}


add_action( 'admin_menu', 'my_plugin_menu' );

/** Step 1. */
function my_plugin_menu() {
	add_options_page( 'My Plugin Options', 'Votes zones', 'manage_options', 'votes-zones', 'my_plugin_options' );
	add_options_page( 'Activation Modal week-end', 'Activation Modal week-end', 'manage_options', 'modal-weekend', 'activation_modal_view' );
	//add_options_page( 'Création RSS MAILCHIMP', 'Création RSS MAILCHIMP','manage_options','rss_mailchimp','rss_mailchimp' );
	add_options_page( 'Créneaux horaires', 'Créneaux horaires', 'manage_options', 'creneaux_horaires', 'creneaux_horaires' );
	add_options_page( 'Stat CSV', 'Stat CSV', 'manage_options', 'stat_csv', 'stat_csv' );
}

function stat_csv(){
	
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri(). '/css/jquery-ui.css', false );
?> 

<script type="text/javascript">
	jQuery( function() {
    var dateFormat = "yy-mm-dd",
      from = jQuery( "#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 3,
          dateFormat : dateFormat
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = jQuery( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        dateFormat : dateFormat
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = jQuery.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
</script>

<div class="wrap">
<form method="post" action="<?php get_site_url() ?>/wp-content/plugins/home/export_csv.php">
	<label for="from">Du</label>
	<input type="text" id="from" name="from">
	<label for="to">au</label>
	<input type="text" id="to" name="to">
	<button>Ok</button>
</form>

</div> <?php 
}

function my_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	global $wpdb;
	$votes = $myrows = $wpdb->get_results( "SELECT zone_id,count(*) as count FROM `vote_zones` group by zone_id order by count DESC" );
	?>
	<div class="wrap">
	<h1>Votes des zones : </h1>

	<table>
	<thead>
		  <tr>
		     <th>Code Postal</th>
		     <th>Nombre de votes</th>
		  </tr>
		 </thead
		 <?php
		foreach ($votes as $key => $vote) {
			echo "<tr><td>".$vote->zone_id."</td><td>".$vote->count."</td></tr>";
		}
	echo '</tables';


	echo '</div>';
}

function activation_modal_view()
{
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	global $wpdb;

	$option = $wpdb->get_row("SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend' LIMIT 1");
	$option = $option->option_value;



	if(isset($_POST['modal_weekend_toogle']))
	{
		if($option == "1")
		{
			$option = "0";
		}
		else
		{
			$option = "1";
		}

		$sql = "update wp_options set option_value = ".$option." where option_name = 'modal_weekend'";

		$wpdb->query($sql);
	}
	else if(isset($_POST['modal_weekend_text_submit']))
	{
		$sql = "update wp_options set option_value = '".$_POST['modal_weekend_text']."' where option_name = 'modal_weekend_text'";

		$wpdb->query($sql);
	}

	if($option == "0")
	{
		$value_name = "Activer la modal week-end";
	}
	else
	{
		$value_name = "Désactiver la modal week-end";
	}



	$sql_option_text = "SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend_text' LIMIT 1";

	$option_text = $wpdb->get_row($sql_option_text);
	$option_text = $option_text->option_value;


	?>
		<div class="wrap">
			<form action='<?php echo site_url(); ?>/wp-admin/options-general.php?page=modal-weekend' method='post'>
				<textarea rows="10" cols="100" name='modal_weekend_text'><?php echo $option_text ?></textarea><br>
				<input name='modal_weekend_text_submit' type='submit' value="Sauvegarder message">
			</form>

			<br>
			<form action='<?php echo site_url(); ?>/wp-admin/options-general.php?page=modal-weekend' method='post'>
				<input name='modal_weekend_toogle' type='submit' value="<?php echo $value_name; ?>">
			</form>
		</div>
	<?php
	
}

function creneaux_horaires()
{
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	global $wpdb;

	if(isset($_POST))
	{
		foreach ($_POST as $key => $schedule) {
			$sql = "update schedules set Availible = '".$schedule."' where id = '".$key."'";
			$wpdb->query($sql);
		}
	}


	$schedules = $wpdb->get_results("SELECT * FROM schedules");

		?>
		<div class="wrap">
			<form method='post'>
				<?php
					foreach ($schedules as $key => $schedule) {
						echo $schedule->hour_from."-".$schedule->hour_to."  : <input type='text' value='".$schedule->Availible."' name='".$schedule->id."'><br>";
					}
				?>
				<button name='submitCreneaux'>OK</button>
			</form>
		</div>	
		<?php
}


function rss_mailchimp()
{
	    $args_entree = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    // 'terms' => 'white-wines'
                    'terms' => "entrees"
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );

        $products_entree = new WP_Query( $args_entree );

       	$args_plat = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    // 'terms' => 'white-wines'
                    'terms' => "plats"
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );

        $products_plat = new WP_Query( $args_plat );

        $args_dessert = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    // 'terms' => 'white-wines'
                    'terms' => "desserts"
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );

        $products_dessert = new WP_Query( $args_dessert );


        var_dump($_POST);

	?>
	<div class="wrap">
		<form action='<?php echo site_url(); ?>/wp-admin/options-general.php?page=rss_mailchimp' method='post'>

		<h1>Entrées :</h1>
		<?php
		echo '<div><select name="e1">';
        foreach ($products_entree->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="e2">';
        foreach ($products_entree->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="e3">';
        foreach ($products_entree->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";
		?>

		<h1>Plat :</h1>
		<?php
		echo '<div><select name="p1">';
        foreach ($products_plat->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="p2">';
        foreach ($products_plat->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="p3">';
        foreach ($products_plat->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";
		?>

		<h1>Dessert :</h1>
		<?php
		echo '<div><select name="d1">';
        foreach ($products_dessert->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="d2">';
        foreach ($products_dessert->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";

        echo '<div><select name="d3">';
        foreach ($products_dessert->posts as $key => $product) {
        	echo '<option>'.$product->post_title.'</option>';
        }
        echo "</select></div>";
		?>

			<input name='rss_mailchimp_submit' type='submit' value="Générer le RSS">
		</form>
	</div>
	<?php
}


function map_livraison(){
	?>
		<div id="map_zone_de_livraison">toto</div>

		<script type="text/javascript">
			jQuery( document ).ready(function() {
				createMapZoneLivraison();
			});
			
		</script>
		
	<?php
}

add_shortcode('map_livraison', 'map_livraison');



function cuisine_ferme(){
		if(current_user_can('administrator'))
		{

		}
		else
		{
					$dayNumber = date('w');

		global $wpdb;
		$option = $wpdb->get_row("SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend' LIMIT 1");
		$option = $option->option_value;

		if($dayNumber == 6 || $dayNumber == 0 || $option == "1")
		{

				$sql_option_text = "SELECT option_value FROM $wpdb->options WHERE option_name = 'modal_weekend_text' LIMIT 1";

				$option_text = $wpdb->get_row($sql_option_text);
				$option_text = $option_text->option_value;

		?>
		<div id="cuisine_ferme">
		<?php echo $option_text; ?></div>
		<?php
		}

	}

}

add_shortcode('cuisine_ferme', 'cuisine_ferme');

