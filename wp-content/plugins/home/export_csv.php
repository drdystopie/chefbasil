<?php

require_once('../../../wp-load.php' );

$period = date_range($_POST['from'],$_POST['to'],'+1 day','Y-m-d');

	if(isset($_POST['from']) && isset($_POST['to']))
	{
		$lines = array();

		foreach ($period as $p) {
		

		    $queryOrders = "SELECT * FROM `wp_posts` WHERE `post_type` LIKE 'shop_order' and post_date > '".$p." 00:00:00' and post_date < '".$p." 29:59:59' and (post_status = 'wc-awaiting-shipment' or post_status = 'wc-completed' or post_status = 'wc-processing' )";

		     $orders = $wpdb->get_results($queryOrders);


			foreach ($orders as $key => $order) {

				$order = new WC_Order($order->ID);

				$first_name = "";
				$last_name = "";
				$phone = "";
				$email = "";

				$table = $wpdb->prefix . 'postmeta';
 				$sql = 'SELECT * FROM `'. $table . '` WHERE post_id = '. $order->id;

 				$user_post_meta = $wpdb->get_results($sql);

 				foreach($user_post_meta as $res) {
 					if($res->meta_key == "_billing_first_name")
 					{
 						$first_name = $res->meta_value;
 					}

 					if($res->meta_key == "_billing_last_name")
 					{
 						$last_name = $res->meta_value;
 					}

 					if($res->meta_key == "_billing_phone")
 					{
 						$phone = "\t".$res->meta_value;
 					}

 					if($res->meta_key == "_billing_email")
 					{
 						$email = $res->meta_value;
 					}


 				}

				$adress = $order->shipping_address_1." ".$order->shipping_address_2." ".$order->shipping_city." ".$order->shipping_postcode;
				
				$schedule = new DateTime();
				$schedule->setTimestamp(get_post_meta( $order->id, 'schedule', true ));

				$schedule_to = new DateTime();
				$schedule_to->setTimestamp(get_post_meta( $order->id, 'schedule', true ));
				$schedule_to->add(new DateInterval('PT30M'));

				$horaire = date("H:i",strtotime($order->post->post_date));
				$date = date("d/m/Y",strtotime($order->post->post_date));

				$coupons = implode(",",$order->get_used_coupons());

				$nbOrder = wc_get_customer_order_count( $order->user_id );

				$sqlNbOrder = "SELECT COUNT(*) as nb
			FROM $wpdb->posts as posts
			LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
			WHERE   meta.meta_key       = '_customer_user' AND meta_value = $order->user_id and post_date > '".$_POST['from']." 00:00:00' and post_date < '".$_POST['to']." 23:59:59' and (post_status = 'wc-awaiting-shipment' or post_status = 'wc-completed' or post_status = 'wc-processing' )";

				$nbOrder = $wpdb->get_row($sqlNbOrder);

				$total = $order->get_total();

				$lines[] = array($order->user_id,
								 $first_name,
								 $last_name,
								 $phone,
								 $email,
								 $adress,
								 $order->shipping_postcode,
								 $schedule->format('H:i')."-".$schedule_to->format('H:i'),
								 $horaire,
								 $p,
								 $total,
								 $order->id,
								 $nbOrder->nb,
								 $coupons);


			}
		}

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=export.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		$header = array_map("utf8_decode", array('ID client', 'Nom', 'Prénom' , 'Téléphone' , 'Email' , 'Adresse', 'Code postal','Créneau de livraison','Horaire','Date','Montant','N°commande','Nombre de commandes','Code Promo'));
		fputcsv($output, $header ,';');

		foreach ($lines as $line) {
			$line = array_map("utf8_decode", $line);
			fputcsv($output, $line,';');
		}
	}