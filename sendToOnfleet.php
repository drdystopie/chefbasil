<?php
if(!empty($_GET['order_id']))
{
	require_once('wp-load.php');
	//Onfleet API credentials
	$api = '06d7c4d45ae367520c9328aaa8a08aa4';


	$order_id = $_GET['order_id'];

	include "./wp-content/themes/vegan/Onfleet.php";
	$config = array("api_name" => "webhooks", "api_key" => $api);
	$onfleet = new Onfleet($config);

	$order = new WC_Order($order_id);

	$order_meta = get_post_meta($order_id);

	$task['destination']['address']['unparsed'] = $order_meta['_shipping_address_1'][0]." ".$order_meta['_shipping_address_2'][0]." ".$order_meta['_shipping_postcode'][0]." ".$order_meta['_shipping_city'][0];

	$task['recipients'][0]['name'] = $order_meta['_billing_first_name'][0]." ".$order_meta['_billing_last_name'][0];
	$task['recipients'][0]['phone'] = $order_meta['_billing_phone'][0];
	$task['recipients'][0]['notes'] = $order->customer_note;

	$timeAfter = $order_meta['schedule'][0] * 1000;

	$task['completeAfter'] = $timeAfter;
	$task['completeBefore'] = $timeAfter + (1800 * 1000);


	$task['autoAssign']['mode'] = 'distance';

	$task['notes'] = $order_id.'|'.$order->customer_note;

	$onfleet->task_create($task);	

	//echo get_site_url()."/wp-admin/post.php?post=".$order_id."&action=edit";

	header('Location: '.get_site_url()."/wp-admin/post.php?post=".$order_id."&action=edit");
}

?>
